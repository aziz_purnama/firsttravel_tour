@extends('layout.master')

@section('title', 'Pesan Tour')
@section('content')
<div class="l-main-container">
  <div class="container">
    <div class="row b-shortcode-example">
      <div class="col-md-offset-3 col-xs-12 col-sm-6">
        <div class="row b-form-inline b-form-horizontal">
          <form action="{{url('/tour-detail/save')}}" id="tourOrder" method="POST" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="id" value="{{$paket->idPaket}}">
          <input type="hidden" name="nama_paket" value="{{$paket->nmPaket}}">
          <input type="hidden" name="total_pesanan" value="{{$request->pesanan}}">
          <input type="hidden" name="harga_satuan" value="{{$request->harga}}">
          <input type="hidden" name="harga_total" value="{{$request->totalHarga}}">
          <div class="col-xs-12">
            <div class="b-form-row f-primary-l f-title-big c-secondary">Data PIC</div>
            <div class="b-form-row">Masukan Data Lengkap Anda :</div>
            <div class="b-form-row">
              <label class="b-form-horizontal__label" for="nama">Nama</label>
              <div class="form-group">
                <input type="text" id="nama" name="nama" class="form-control" required>
              </div>
            </div>
            <div class="b-form-row">
              <label class="b-form-horizontal__label" for="tgl_lahir">Tanggal Lahir</label>
              <div class="form-group">
                <input type="date" id="tgl_lahir" name="tgl_lahir" class="form-control" required>
              </div>
            </div>
            <div class="b-form-row">
              <label class="b-form-horizontal__label" for="jns_id">Jenis ID</label>
              <div class="form-group">
                <select id="jns_id" name="jns_id" class="form-control" required>
                  <option value="KTP">KTP</option>
                  <option value="SIM">SIM</option>
                </select>
              </div>
            </div>
            <div class="b-form-row">
              <label class="b-form-horizontal__label" for="no_id">No ID</label>
              <div class="form-group">
                <input type="text" id="no_id" name="no_id" class="form-control" required>
              </div>
            </div>
            <div class="b-form-row">
              <label class="b-form-horizontal__label" for="passport">Passport</label>
              <div class="form-group">
                <input type="text" id="passport" name="passport" class="form-control" required>
              </div>
            </div>
            <div class="b-form-row">
              <label class="b-form-horizontal__label" for="email">Email</label>
              <div class="form-group">
                <input type="email" id="email" name="email" class="form-control" required>
              </div>
            </div>
            <div class="b-form-row">
              <label class="b-form-horizontal__label" for="no_tlp">No Tlp</label>
              <div class="form-group">
                <input type="text" id="no_tlp" name="no_tlp" class="form-control" required>
              </div>
            </div>
            <div class="b-form-row">
              <label class="b-form-horizontal__label" for="jns_pembayaran">Jenis Pembayaran</label>
              <div class="form-group">
                <input type="text" id="jns_pembayaran" name="jns_pembayaran" class="form-control" value="TRANSFER" readonly="readonly" required>
              </div>
            </div>
            <hr class="b-hr">
            <div class="b-form-row f-primary-l f-title-big c-secondary">Data Detail</div>
            @for ($i = 1; $i <= $request->pesanan; $i++)
            <hr class="b-hr">
            <h2>Data  <span class="label label-info">{{$i}}</span></h2>
            <div class="b-form-row">
              <label class="b-form-horizontal__label" for="namaDetail[]">Nama</label>
              <div class="form-group">
                <input type="text" id="namaDetail[]" name="namaDetail[]" class="form-control" required>
              </div>
            </div>
            <div class="b-form-row">
              <label class="b-form-horizontal__label" for="tglLahir[]">Tanggal Lahir</label>
              <div class="form-group">
                <input type="date" id="tglLahir[]" name="tglLahir[]" class="form-control" required>
              </div>
            </div>
            @endfor
            <div class="b-form-row">
              <div class="b-form-horizontal__label"></div>
              <div class="form-group">
                <label for="create_account_terms">
                  <input type="checkbox" class="b-form-checkbox b-form-checkbox" id="create_account_terms" required>
                  <span>I agree to <a href="#" class="c-secondary f-more">Terms of Use</a></span>
                </label>
              </div>
            </div>
            <div class="b-form-row">
              <div class="b-form-horizontal__label"></div>
              <div class="form-group">
                <button class="b-btn f-btn b-btn-md b-btn-default f-primary-b b-btn__w100">Pesan Sekarang</button>
              </div>
            </div>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection