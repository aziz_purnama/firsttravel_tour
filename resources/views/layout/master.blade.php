<!doctype html>
<!--[if lt IE 7]><html lang="en" class="no-js ie6"><![endif]-->
<!--[if IE 7]><html lang="en" class="no-js ie7"><![endif]-->
<!--[if IE 8]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <title>First Travel Tour - @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="shortcut icon" href="#">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    @yield('cascanding')
    <link rel="shortcut icon" href="#">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	<!-- bxslider -->
	<link type="text/css" rel='stylesheet' href="{{asset('assets/js/bxslider/jquery.bxslider.css')}}">
	<!-- End bxslider -->
	<!-- flexslider -->
	<link type="text/css" rel='stylesheet' href="{{asset('assets/js/flexslider/flexslider.css')}}">
	<!-- End flexslider -->

	<!-- bxslider -->
	<link type="text/css" rel='stylesheet' href="{{asset('assets/js/radial-progress/style.css')}}">
	<!-- End bxslider -->

	<!-- Animate css -->
	<link type="text/css" rel='stylesheet' href="{{asset('assets/css/animate.css')}}">
	<!-- End Animate css -->

	<!-- Bootstrap css -->
	<link type="text/css" rel='stylesheet' href="{{asset('assets/css/bootstrap.min.css')}}">
	<link type="text/css" rel='stylesheet' href="{{asset('assets/js/bootstrap-progressbar/bootstrap-progressbar-3.2.0.min.css')}}">
	<!-- End Bootstrap css -->

	<!-- Jquery UI css -->
	<link type="text/css" rel='stylesheet' href="{{asset('assets/js/jqueryui/jquery-ui.css')}}">
	<link type="text/css" rel='stylesheet' href="{{asset('assets/js/jqueryui/jquery-ui.structure.css')}}">
	<!-- End Jquery UI css -->

	<!-- Fancybox -->
	<link type="text/css" rel='stylesheet' href="{{asset('assets/js/fancybox/jquery.fancybox.css')}}">
	<!-- End Fancybox -->

	<link type="text/css" rel='stylesheet' href="{{asset('assets/fonts/fonts.css')}}">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

	<link type="text/css" data-themecolor="default" rel='stylesheet' href="{{asset('assets/css/main-default.css')}}">

	<link type="text/css" rel='stylesheet' href="{{asset('assets/js/rs-plugin/css/settings.css')}}">
	<link type="text/css" rel='stylesheet' href="{{asset('assets/js/rs-plugin/css/settings-custom.css')}}">
	<link type="text/css" rel='stylesheet' href="{{asset('assets/js/rs-plugin/videojs/video-js.css')}}">

</head>

<body>
    @include('layout.header')
    <div class="j-menu-container"></div>
    @yield('slider-details')
    <!-- <div class="j-menu-container"></div>
    <div class="l-main-container"> -->
    @yield('content')
    <!-- </div> -->
    @include('layout.footer')
    <script src="{{asset('assets/js/breakpoints.js')}}"></script>
	<script src="{{asset('assets/js/jquery/jquery-1.11.1.min.js')}}"></script>
	<!-- bootstrap -->
	<script src="{{asset('assets/js/scrollspy.js')}}"></script>
	<script src="{{asset('assets/js/bootstrap-progressbar/bootstrap-progressbar.js')}}"></script>
	<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
	<!-- end bootstrap -->
	<script src="{{asset('assets/js/masonry.pkgd.min.js')}}"></script>
	<script src="{{asset('assets/js/imagesloaded.pkgd.min.js')}}"></script>
	<!-- bxslider -->
	<script src="{{asset('assets/js/bxslider/jquery.bxslider.min.js')}}"></script>
	<!-- end bxslider -->
	<!-- flexslider -->
	<script src="{{asset('assets/js/flexslider/jquery.flexslider.js')}}"></script>
	<!-- end flexslider -->
	<!-- smooth-scroll -->
	<script src="{{asset('assets/js/smooth-scroll/SmoothScroll.js')}}"></script>
	<!-- end smooth-scroll -->
	<!-- carousel -->
	<script src="{{asset('assets/js/jquery.carouFredSel-6.2.1-packed.js')}}"></script>
	<!-- end carousel -->
	<script src="{{asset('assets/js/rs-plugin/js/jquery.themepunch.plugins.min.js')}}"></script>
	<script src="{{asset('assets/js/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
	<script src="{{asset('assets/js/rs-plugin/videojs/video.js')}}"></script>

	<!-- jquery ui -->
	<script src="{{asset('assets/js/jqueryui/jquery-ui.js')}}"></script>
	<!-- end jquery ui -->
	<script type="text/javascript" language="javascript"
	        src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCfVS1-Dv9bQNOIXsQhTSvj7jaDX7Oocvs"></script>
	<!-- Modules -->
	<script src="{{asset('assets/js/modules/sliders.js')}}"></script>
	<script src="{{asset('assets/js/modules/ui.js')}}"></script>
	<script src="{{asset('assets/js/modules/retina.js')}}"></script>
	<script src="{{asset('assets/js/modules/animate-numbers.js')}}"></script>
	<script src="{{asset('assets/js/modules/parallax-effect.js')}}"></script>
	<script src="{{asset('assets/js/modules/settings.js')}}"></script>
	<script src="{{asset('assets/js/modules/maps-google.js')}}"></script>
	<script src="{{asset('assets/js/modules/color-themes.js')}}"></script>
	<!-- End Modules -->

	<!-- Audio player -->
	<script type="text/javascript" src="{{asset('assets/js/audioplayer/js/jquery.jplayer.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/audioplayer/js/jplayer.playlist.min.js')}}"></script>
	<script src="{{asset('assets/js/audioplayer.js')}}"></script>
	<!-- end Audio player -->

	<!-- radial Progress -->
	<script src="{{asset('assets/js/radial-progress/jquery.easing.1.3.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.4.13/d3.min.js"></script>
	<script src="{{asset('assets/js/radial-progress/radialProgress.js')}}"></script>
	<script src="{{asset('assets/js/progressbars.js')}}"></script>
	<!-- end Progress -->

	<!-- Google services -->
	<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart']}]}"></script>
	<script src="{{asset('assets/js/google-chart.js')}}"></script>
	<!-- end Google services -->
	<script src="{{asset('assets/js/j.placeholder.js')}}"></script>

	<!-- Fancybox -->
	<script src="{{asset('assets/js/fancybox/jquery.fancybox.pack.js')}}"></script>
	<script src="{{asset('assets/js/fancybox/jquery.mousewheel.pack.js')}}"></script>
	<script src="{{asset('assets/js/fancybox/jquery.fancybox.custom.js')}}"></script>
	<!-- End Fancybox -->
	<script src="{{asset('assets/js/user.js')}}"></script>
	<script src="{{asset('assets/js/timeline.js')}}"></script>
	<script src="{{asset('assets/js/fontawesome-markers.js')}}"></script>
	<script src="{{asset('assets/js/markerwithlabel.js')}}"></script>
	<script src="{{asset('assets/js/cookie.js')}}"></script>
	<script src="{{asset('assets/js/loader.js')}}"></script>
	<script src="{{asset('assets/js/scrollIt/scrollIt.min.js')}}"></script>
	<script src="{{asset('assets/js/modules/navigation-slide.js')}}"></script>
	<script src="{{asset('assets/js/typeahead.bundle.js')}}"></script>
	@yield('javascript')
</body>

</html>
