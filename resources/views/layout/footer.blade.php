<footer>
  <div class="b-footer-primary">
    <div class="container">
      <div class="row">
        <div class="col-sm-4 col-xs-12 f-copyright b-copyright">Copyright © 2017 - All Rights Reserved</div>
        <div class="col-sm-8 col-xs-12">
          <div class="b-btn f-btn b-btn-default b-right b-footer__btn_up f-footer__btn_up j-footer__btn_up">
            <i class="fa fa-chevron-up"></i>
          </div>
          <nav class="b-bottom-nav f-bottom-nav b-right hidden-xs">
            <ul>
                <li><a href="{{ url('/') }}">Homepage</a></li>
                <li class="is-active-bottom-nav"><a href="{{url('/tours')}}">Tours</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="b-footer-secondary row">
      <div class="col-md-4 col-sm-12 col-xs-12 f-center b-footer-logo-containter">
        <a href="">
          <img data-retina class="b-footer-logo color-theme" src="{{asset('assets/img/logo-vertical-default.jpg')}}" alt="Logo"/>
        </a>
        <div class="b-footer-logo-text f-footer-logo-text">
          <div class="b-btn-group-hor f-btn-group-hor">
            <a href="#" class="b-btn-group-hor__item f-btn-group-hor__item">
              <i class="fa fa-twitter"></i>
            </a>
            <a href="#" class="b-btn-group-hor__item f-btn-group-hor__item">
              <i class="fa fa-facebook"></i>
            </a>
            <a href="#" class="b-btn-group-hor__item f-btn-group-hor__item">
              <i class="fa fa-instagram"></i>
            </a>
          </div>
          <br>
          <img src="{{asset('assets/img/logo/icon-footer-visitor.png')}}" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);">Visitor&nbsp;:&nbsp;<span style="color:#006699 !important;">92</span>
          <!-- <div align='center'><a href='http://www.hit-counts.com'><img src='http://www.hit-counts.com/counter.php?t=MTQwNjQ3Mg==' border='0' alt='Visitor Counter'></a><BR><a href='http://www.hit-counts.com'></a><a href="https://www.pixelmeta.com/corporate-identity-designing/" title="online graphic design services" style="visibility: hidden; display: none; width: 0; height: 0; z-index: -5000;">online graphic design services</a></div> -->
        </div>
      </div>
      <!-- <div class="col-md-3 col-sm-12 col-xs-12">
        <h4 class="f-primary-b">News</h4>
        <div class="b-blog-short-post row">
          <div class="b-blog-short-post__item col-md-12 col-sm-4 col-xs-12 f-primary-b">
            <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
             <a href="firsttravel.co.id?mod=news" target="blank_">RIBUAN JAMAAH HADIRI MANASIK UMROH 2017 DAN DZIKIR AKBAR</a>
            </div>
            <div class="b-blog-short-post__item_date f-blog-short-post__item_date"></div>
          </div>
        </div>
        <div class="b-blog-short-post__item col-md-12 col-sm-4 col-xs-12 f-primary-b">
          <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
            <a href="firsttravel.co.id?mod=news" target="blank_">40 RIBU JAMAAH IKUTI MANASIK UMRAH FIRST TRAVEL</a>
          </div>
          <div class="b-blog-short-post__item_date f-blog-short-post__item_date"></div>
        </div>
      </div> -->
      <div class="col-md-4 col-sm-12 col-xs-12">
        <h4 class="f-primary-b">CONTAC INFO</h4>
        <div class="b-contacts-short-item-group">
          <div class="b-contacts-short-item col-md-12 col-sm-4 col-xs-12">
            <div class="b-contacts-short-item__icon f-contacts-short-item__icon f-contacts-short-item__icon_lg b-left">
              <i class="fa fa-map-marker"></i>
            </div>
            <div class="b-remaining f-contacts-short-item__text">
              GKM Green Tower,Lantai 16<br> 
            </div>
          </div>
          <div class="b-contacts-short-item col-md-12 col-sm-4 col-xs-12">
            <div class="b-contacts-short-item__icon f-contacts-short-item__icon b-left f-contacts-short-item__icon_md">
              <i class="fa fa-phone"></i>
            </div>
            <div class="b-remaining f-contacts-short-item__text f-contacts-short-item__text_phone">
              <p>+6221 8063 3707</p>  
            </div>
          </div>
          <div class="b-contacts-short-item col-md-12 col-sm-4 col-xs-12">
            <div class="b-contacts-short-item__icon f-contacts-short-item__icon b-left f-contacts-short-item__icon_xs">
              <i class="fa fa-envelope"></i>
            </div>
            <div class="b-remaining f-contacts-short-item__text f-contacts-short-item__text_email">
              <a href="mailto:frexystudio@gmail.com">info@firsttravel.co.id</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-12 col-xs-12 ">
        <h4 class="f-primary-b"></h4>
        <div class="b-short-photo-items-group">
          <div class="b-column">
            <a class="b-short-photo-item fancybox margin-top: 20%;" style="margin-top: 20%;" href="{{asset('assets/img/logo/asita.jpg')}}" title="photo stream" rel="footer-group"><img height="62" data-retina="" alt="" src="{{asset('assets/img/logo/asita.jpg')}}" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);" width="64"></a>
          </div>
          <div class="b-column">
            <a class="b-short-photo-item fancybox margin-top: 20%;" style="margin-top: 20%;" href="{{asset('assets/img/logo/iata.jpg')}}" title="photo stream" rel="footer-group"><img height="62" data-retina="" alt="" src="{{asset('assets/img/logo/iata.jpg')}}" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);" width="64"></a>
          </div>
          <div class="b-column">
            <a class="b-short-photo-item fancybox margin-top: 20%;" style="margin-top: 20%;" href="{{asset('assets/img/logo/wqa.jpg')}}" title="photo stream" rel="footer-group"><img height="62" data-retina="" alt="" src="{{asset('assets/img/logo/wqa.jpg')}}" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);" width="64"></a>
          </div>
          <div class="b-column">
            <a class="b-short-photo-item fancybox margin-top: 20%;" style="margin-top: 20%;" href="{{asset('assets/img/logo/ft_foundation.png')}}" title="photo stream" rel="footer-group"><img height="62" data-retina="" alt="" src="{{asset('assets/img/logo/ft_foundation.png')}}" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);" width="64"></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>