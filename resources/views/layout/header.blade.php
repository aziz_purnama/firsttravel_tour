<div class="mask-l" style="background-color: #fff; width: 100%; height: 100%; position: fixed; top: 0; left:0; z-index: 9999999;">
</div> 
<!--removed by integration-->
<header>
  <div class="container b-header__box b-relative">
    <a href="/" class="b-left b-logo ">
      <img class="color-theme" data-retina src="{{asset('assets/img/logo-header-default.png')}}" alt="Logo" width="80%" />
    </a>
    <div class="b-header-r b-left b-header-r--icon">
      <div class="b-top-nav-show-slide f-top-nav-show-slide b-right j-top-nav-show-slide">
        <i class="fa fa-align-justify">
        </i>
      </div>
      <nav class="b-top-nav f-top-nav b-right j-top-nav">
        <ul class="b-top-nav__1level_wrap">
          <li class="b-top-nav__1level f-top-nav__1level is-active-top-nav__1level f-primary-b">
            <a href="{{url('/')}}">
              <i class="fa fa-home b-menu-1level-ico">
              </i>Home 
            </a>
          </li>
          <li class="b-top-nav__1level f-top-nav__1level f-primary-b">
            <a href="page_header_1.html">
              <i class="fa fa-folder-open b-menu-1level-ico">
              </i>Tour
              <span class="b-ico-dropdown">
                <i class="fa fa-arrow-circle-down">
                </i>
              </span>
            </a>
            <div class="b-top-nav__dropdomn">
              <ul class="b-top-nav__2level_wrap">
                <li class="b-top-nav__2level_title f-top-nav__2level_title">Tour
                </li>
                <li class="b-top-nav__2level f-top-nav__2level f-primary">
                  <a href="{{ url('tour-list-internasional') }}">
                    <i class="fa fa-angle-right">
                    </i>Tour Internasional
                  </a>
                </li>
                <li class="b-top-nav__2level f-top-nav__2level f-primary">
                  <a href="{{ url('tour-list-domestik') }}">
                    <i class="fa fa-angle-right">
                    </i>Tour Domestik
                  </a>
                </li>
              </ul>
            </div>
          </li>
          <!-- <li class="b-top-nav__1level f-top-nav__1level f-primary-b">
            <a href="{{url('/konfirmasi')}}">
              <i class="fa fa-home b-menu-1level-ico">
              </i>Konfirmasi 
            </a>
          </li> -->
        </ul>
      </nav>
    </div>
  </div>
</header>
