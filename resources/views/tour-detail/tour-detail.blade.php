@extends('layout.master')

@section('title', 'Tour Internasional')
@section('content')
<div class="l-main-container">
<div class="b-breadcrumbs f-breadcrumbs">
  <div class="container">
    <ul>
      <li>
        <a href="#">
          <i class="fa fa-home">
          </i>Home
        </a>
      </li>
      <li>
        <i class="fa fa-angle-right">
        </i>
        <a href="#"> Travel
        </a>
      </li>
      <li>
        <i class="fa fa-angle-right">
        </i>
        <span>Tours Details
        </span>
      </li>
    </ul>
  </div>
</div>
<div class="l-inner-page-container">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="b-slider b-slider--innerbullet">
          <div class="flexslider flexslider-zoom">
            <ul class="slides">
              @foreach($foto as $fotos)
              <li>
                <img data-retina src="{{asset($fotos->file_foto)}}">
              </li>
              @endforeach
              <!-- <li>
                <img data-retina src="{{asset('assets/img/travel/travel-slider-lg-2.jpg')}}">
              </li>
              <li>
                <img data-retina src="{{asset('assets/img/travel/travel-slider-lg-3.jpg')}}">
              </li>
              <li>
                <img data-retina src="{{asset('assets/img/travel/travel-slider-lg-4.jpg')}}">
              </li>
              <li>
                <img data-retina src="{{asset('assets/img/travel/travel-slider-lg-5.jpg')}}">
              </li>
              <li>
                <img data-retina src="{{asset('assets/img/travel/travel-slider-lg-6.jpg')}}">
              </li>
              <li>
                <img data-retina src="{{asset('assets/img/travel/travel-slider-lg-7.jpg')}}">
              </li>
              <li>
                <img data-retina src="{{asset('assets/img/travel/travel-slider-lg-8.jpg')}}">
              </li>
              <li>
                <img data-retina src="{{asset('assets/img/travel/travel-slider-lg-9.jpg')}}">
              </li>
              <li>
                <img data-retina src="{{asset('assets/img/travel/travel-slider-lg-10.jpg')}}">
              </li>
              <li>
                <img data-retina src="{{asset('assets/img/travel/travel-slider-lg-1.jpg')}}">
              </li>
              <li>
                <img data-retina src="{{asset('assets/img/travel/travel-slider-lg-2.jpg')}}">
              </li> -->
            </ul>
          </div>
          <div class="flexslider flexslider-thumbnail carousel-md">
            <ul class="slides">
              <!-- <li>
                <img src="{{asset('assets/img/travel/sm/travel-slider-sm-1.jpg')}}" />
              </li>
              <li>
                <img src="{{asset('assets/img/travel/sm/travel-slider-sm-2.jpg')}}" />
              </li>
              <li>
                <img src="{{asset('assets/img/travel/sm/travel-slider-sm-3.jpg')}}" />
              </li>
              <li>
                <img src="{{asset('assets/img/travel/sm/travel-slider-sm-4.jpg')}}" />
              </li>
              <li>
                <img src="{{asset('assets/img/travel/sm/travel-slider-sm-5.jpg')}}" />
              </li>
              <li>
                <img src="{{asset('assets/img/travel/sm/travel-slider-sm-6.jpg')}}" />
              </li>
              <li>
                <img src="{{asset('assets/img/travel/sm/travel-slider-sm-7.jpg')}}" />
              </li>
              <li>
                <img src="{{asset('assets/img/travel/sm/travel-slider-sm-8.jpg')}}" />
              </li>
              <li>
                <img src="{{asset('assets/img/travel/sm/travel-slider-sm-9.jpg')}}" />
              </li>
              <li>
                <img src="{{asset('assets/img/travel/sm/travel-slider-sm-10.jpg')}}" />
              </li>
              <li>
                <img src="{{asset('assets/img/travel/sm/travel-slider-sm-1.jpg')}}" />
              </li> -->
              @foreach($foto as $fotos)
              <li>
                <img src="{{asset($fotos->file_foto)}}">
              </li>
              @endforeach
            </ul>
          </div>
        </div>
        <div class="b-article-box">
          <div class="f-primary-l f-primary-title b-primary-title">{{$paket->nmPaket}}
          </div>
          <div class="b-portfolio_info_rating">
            <!-- <div class="b-portfolio_rating_category">
              <span class="f-portfolio_category_title">
                <i class="fa fa-clock-o">
                </i> Duration: 11 Nov 2013 - 22 Nov 2013
              </span>
            </div> -->
            <!-- <span class="b-rating_bord hidden-xs">
            </span> -->
            <!-- <div class="b-portfolio_rating_category">
              <span class="f-portfolio_category_title">Location: 
              </span>
              <a href="#" class="f-portfolio_category_name">America
              </a>
            </div> -->
            <!-- <span class="b-rating_bord hidden-xs">
            </span> -->
            <div class="b-portfolio_rating_category">
              <span class="f-uppercase c-default f-primary-b">Price: IDR {{number_format($paket->hrgPaket, 2, ',', '.')}} 
                <input name="harga-satuan" id="harga-satuan" value="{{$paket->hrgPaket}}" type="hidden">
              </span>
            </div>
            <span class="b-rating_bord hidden-xs">
            </span>
            <div class="b-portfolio_rating_category">
              <span class="f-portfolio_category_title c-default">Rate it
              </span>
              <span class="b-stars-group f-stars-group">
                <i class="fa fa-star is-active-stars">
                </i>
                <i class="fa fa-star is-active-stars">
                </i>
                <i class="fa fa-star is-active-stars">
                </i>
                <i class="fa fa-star is-active-stars">
                </i>
                <i class="fa fa-star">
                </i>
              </span>
            </div>
          </div>
          <div class="b-article__description">
            <p>{!!$paket->desPaket!!}
            </p>            
          </div>
          <div class="b-blog-form-box">
            <div class="row">
              <div class="col-md-3">
                <label class="b-form-vertical__label" for="qty">Jumlah Pesanan :</label>
                <div class="b-form-vertical__input input-group">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button" id="mins"><li class="fa fa-minus"></li></button>
                  </span>
                  <input type="text" id="qty" class="form-control" placeholder="Qty" value="0" type="number">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button" id="plus"><li class="fa fa-plus"></li></button>
                    <button class="btn btn-primary" type="button" id="pesan-now" data-toggle="modal" data-target="#myModal">Pesan</button>
                  </span>
                </div>
              </div>  
            </div>
          </div><br>
          <div class="b-tabs f-tabs j-tabs b-tabs-reset b-tabs--secondary f-tabs--secondary">
            <ul class="f-left">
              <li>
                <a href="#tabs-31">Tour details
                </a>
              </li>
            </ul>
            <div class="b-tabs__content">
              <div id="tabs-31" class="clearfix">
                <div class="row b-daily-row">
                  <div class="col-sm-6">
                    <div class="b-daily-wrap">
                      {!! $paket->Itinerary !!}
                     <!--  <div class="b-daily_day f-daily_day f-primary-b">day 1
                      </div>
                      <div class="b-daily_description f-daily_description">
                        
                      </div> -->
                    </div>
                  </div>
                  <!-- <div class="col-sm-6">
                    <div class="b-daily-wrap">
                      <div class="b-daily_day f-daily_day f-primary-b">day 2
                      </div>
                      <div class="b-daily_description f-daily_description">
                        Aenean eu leo quam pellentesque ornare. Sem lacinia quam venenatis vestibulum. Donec ullamcorper nulla non metus auctor fringilla. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Nullam quis risus eget urna mollis ornare
                      </div>
                    </div>
                  </div>
                  <div class="clearfix visible-sm visible-md visible-lg">
                  </div>
                  <div class="col-sm-6">
                    <div class="b-daily-wrap">
                      <div class="b-daily_day f-daily_day f-primary-b">day 3
                      </div>
                      <div class="b-daily_description f-daily_description">
                        Aenean eu leo quam pellentesque ornare. Sem lacinia quam venenatis vestibulum. Donec ullamcorper nulla non metus auctor fringilla. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Nullam quis risus eget urna mollis ornare
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="b-daily-wrap">
                      <div class="b-daily_day f-daily_day f-primary-b">day 4
                      </div>
                      <div class="b-daily_description f-daily_description">
                        Aenean eu leo quam pellentesque ornare. Sem lacinia quam venenatis vestibulum. Donec ullamcorper nulla non metus auctor fringilla. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Nullam quis risus eget urna mollis ornare
                      </div>
                    </div>
                  </div> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <form action="{{url('/tour-detail/submit/'.$paket->idPaket)}}" id="tourOrder" method="POST" enctype="multipart/form-data">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Konfirmasi Order </h4>
      </div>
      <div class="modal-body" id="confrim-body">
        <div class="b-form-row">
          <label class="b-form-horizontal__label" for="harga">Harga</label>
          <div class="form-group">
            <input type="text" id="harga" name="harga" class="form-control" readonly="readonly">
          </div>
        </div>
        <div class="b-form-row">
          <label class="b-form-horizontal__label" for="pesanan">Jumlah Pesanan</label>
          <div class="form-group">
            <input type="text" id="pesanan" name="pesanan" class="form-control" readonly="readonly">
          </div>
        </div>
        <div class="b-form-row">
          <label class="b-form-horizontal__label" for="totalHarga">Total Harga</label>
          <div class="form-group">
            <input type="text" id="totalHarga" name="totalHarga" class="form-control" readonly="readonly">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-success">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    </form>
  </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
  $('#mins').click(function() {
    var val = parseInt($('#qty').val());
    if($('#qty').val() > 0){
      $('#qty').val(val - (1));
    }else{
      $('#qty').val(0);
    }
  });
  $('#plus').click(function() {
    var val = parseInt($('#qty').val());
    $('#qty').val(val + (1));
  });
  $('#pesan-now').click(function() {
    var qty   = parseInt($('#qty').val());
    var harga = parseInt($('#harga-satuan').val());
    var totalHarga = qty * harga;
    $('#totalHarga').val(totalHarga);
    $('#harga').val(harga);
    $('#pesanan').val(qty);

  });

</script>
@endsection