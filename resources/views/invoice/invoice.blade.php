@extends('layout.master')
@section('title', 'Invoice')
@section('cascanding')
<style type="text/css">
  .invoice {
    position: relative;
    background: #fff;
    border: 1px solid #f4f4f4;
    padding: 20px;
    margin: 10px 25px;
  }
  .page-header {
    margin: 10px 0 20px 0;
    font-size: 22px;
  }
</style>
@endsection
@section('content')
<section class="content content_content" style="width: 70%; margin: auto;">
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <img src="{{asset('assets/img/logo-header-default.png')}}"> First Travel Tour
          <small class="pull-right">{{$paket->tglTrans}}
          </small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        From
        <address>
          <strong>
          First Travel Tour
          </strong>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        To
        <address>
          <strong>
            {{$paket->nmTraveler}}                                    
          </strong>                                  
          <br>
          Phone:
          {{$paket->tlpTraveler}}                                                      
          <br>
          Email:{{$paket->emailTraveler}}                                 
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        <b>Invoice #{{$paket->noTrans}}
        </b>
        <br>
        <br>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Paket
              </th>
              <th>Qty
              </th>
              <th>Price
              </th>
              <th>Sub Total
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{{$paket->nmPaket}}
              </td>
              <td>{{$paket->jmlPax}}
              </td>
              <td>{{$paket->hrgPaketSatuan}}
              </td>
              <td>{{$paket->hrgTotal}}
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    <div class="row">
      <!-- accepted payments column -->
      <div class="col-md-12">
        <!-- <p class="lead">Amount Due 2/22/2014
        </p> -->
        <div class="table-responsive">
          <table class="table">
            <tbody>
              <tr>
                <th>Total:
                </th>
                <td> {{$paket->hrgTotal}}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <button href="" class="btn btn-default" id="print">
          <i class="fa fa-print">
          </i> Print
        </button>
        <button class="btn btn-success pull-right">
          <i class="fa fa-credit-card">
          </i> Konfirmasi Pembayaran
        </button>
        <a href="{{url('/print/'.$paket->noTrans)}}" class="btn btn-primary pull-right" style="margin-right: 5px;">
          <i class="fa fa-download">
          </i> Generate PDF
        </a>
      </div>
    </div>
  </section>
</section>
@endsection
@section('javascript')
<script type="text/javascript">
  $('#print').click(function() {
    window.print();
  });
</script>
@endsection
