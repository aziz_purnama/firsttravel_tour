@extends('layout.master')

@section('title', 'Pesan Tour')
@section('content')
<div class="l-main-container" style="padding-bottom:180px">
  <div class="container">
    <div class="row b-shortcode-example">
      <div class="col-md-offset-3 col-xs-12 col-sm-6">
        <div class="row b-form-inline b-form-horizontal">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="col-xs-12">
            <div class="b-form-row f-primary-l f-title-big c-secondary">Konfirmasi Pembayaran</div>
            <div class="b-form-row">Masukan Nomor Invoice Anda :</div><br>
            <div class="b-form-row">
              <label class="b-form-horizontal__label" for="invoice">Nomor Invoice</label>
              <div class="b-form-vertical__input input-group">
                <input type="text" id="invoice" class="form-control" placeholder="Invoice Number">
                <span class="input-group-btn">
                  <button class="btn btn-primary" type="button" id="submit-inv" data-toggle="modal" >Submit</button>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div id="modalKonfirmasi" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <form action="" id="tourOrder" method="POST" enctype="multipart/form-data">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Konfirmasi Order </h4>
      </div>
      <div class="modal-body" id="confrim-body">
        <div class="b-form-row">
          <label class="b-form-horizontal__label" for="harga">Harga</label>
          <div class="form-group">
            <input type="text" id="harga" name="harga" class="form-control" readonly="readonly">
          </div>
        </div>
        <div class="b-form-row">
          <label class="b-form-horizontal__label" for="pesanan">Jumlah Pesanan</label>
          <div class="form-group">
            <input type="text" id="pesanan" name="pesanan" class="form-control" readonly="readonly">
          </div>
        </div>
        <div class="b-form-row">
          <label class="b-form-horizontal__label" for="totalHarga">Total Harga</label>
          <div class="form-group">
            <input type="text" id="totalHarga" name="totalHarga" class="form-control" readonly="readonly">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-success">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    </form>
  </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
  $('#submit-inv').click(function() {
    // $.getJSON("demo_ajax_json.js", function(result){
    //     $.each(result, function(i, field){
    //         $("div").append(field + " ");
    //     });
    // });
    $('#modalKonfirmasi').modal({show:true});
  });
</script>
@endsection