@extends('layout.master')
@section('title', 'Tour Domestik')
@section('slider-details')
<div class="l-main-container">
<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page">
  <div class="b-inner-page-header__content">
    <div class="container">
      <h1 class="f-primary-l c-default">Our tours</h1>
      <div class="f-primary-l f-inner-page-header_title-add c-senary">Tour Domestik</div>
    </div>
  </div>
</div>
@endsection
@section('content')
<div class="b-breadcrumbs f-breadcrumbs">
  <div class="container">
    <ul>
      <li>
        <a href="{{ url('tour-list') }}">
          <i class="fa fa-home">
          </i>Home
        </a>
      </li>
      <li>
        <i class="fa fa-angle-right">
        </i>
        <a href="#"> Travel
        </a>
      </li>
      <li>
        <i class="fa fa-angle-right">
        </i>
        <span> Tour Domestik
        </span>
      </li>
    </ul>
  </div>
</div>
<section class="b-infoblock--small">
  <div class="container">
    <div class="row b-col-default-indent f-some-examples b-some-examples-tertiary f-some-examples-quaternary ">
       @foreach ($paket as $pakets)
      <div class="col-md-4 col-sm-6">
        <div class="b-some-examples__item f-some-examples__item">
          <div class="b-some-examples__item_img view view-sixth">
            <img data-retina="" src="{{$pakets->file_foto}}" alt="{{$pakets->file_foto}}">
            <div class="b-item-hover-action f-center mask">
              <div class="b-item-hover-action__inner">
                <div class="b-item-hover-action__inner-btn_group">
                  <a href="{{url('/tour-detail/'.$pakets->idPaket)}}" class="b-btn f-btn b-btn-light f-btn-light info">
                    <i class="fa fa-link">
                    </i>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="b-some-examples__item_info">
            <div class="b-some-examples__item_info_level b-some-examples__item_name f-some-examples__item_name f-primary-b">
              <a href="{{url('/tour-detail/'.$pakets->idPaket)}}">{{$pakets->nmPaket}}
              </a>
            </div>
            <div class="b-some-examples__item_info_level b-some-examples__item_double_info f-some-examples__item_double_info clearfix">
              <div class="b-stars-group f-stars-group">
                <i class="fa fa-star is-active-stars">
                </i>
                <i class="fa fa-star is-active-stars">
                </i>
                <i class="fa fa-star is-active-stars">
                </i>
                <i class="fa fa-star is-active-stars">
                </i>
                <i class="fa fa-star">
                </i>
              </div>
            </div>
            <div class="b-some-examples__item_info_level b-some-examples__item_description f-some-examples__item_description">
              {!!$pakets->desPaket!!}
            </div>
          </div>
          <div class="b-some-examples__item_action">
            <div class="b-right">
              <a href="{{url('/tour-detail/'.$pakets->idPaket)}}" class="b-btn f-btn b-btn-sm b-btn-default f-primary-b">Book now
              </a>
            </div>
            <div class="b-remaining b-some-examples__item_total f-some-examples__item_total f-primary-b f-uppercase">From IDR {{number_format($pakets->hrgPaket, 2, ',', '.')}}
            </div>
          </div>
        </div>
      </div>
      @endforeach
    </div>
    <div class="b-shortcode-example b-null-bottom-indent">
      <div class="b-pagination f-pagination">
        {{ $paket->appends(['sort' => 'Internasional'])->links('vendor.pagination.default') }}
      </div>
    </div>
  </div>
</section>
</div>
@endsection
