@extends('layout.master')
@section('title', 'Home')
@section('cascanding')
<style type="text/css">
.tt-query {
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
     -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
}

.tt-hint {
  color: #999
}

.tt-menu {    /* used to be tt-dropdown-menu in older versions */
  width: 422px;
  margin-top: 4px;
  padding: 4px 0;
  background-color: #fff;
  border: 1px solid #ccc;
  border: 1px solid rgba(0, 0, 0, 0.2);
  -webkit-border-radius: 4px;
     -moz-border-radius: 4px;
          border-radius: 4px;
  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
     -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
          box-shadow: 0 5px 10px rgba(0,0,0,.2);
}

.tt-suggestion {
  padding: 3px 20px;
  line-height: 24px;
}

.tt-suggestion.tt-cursor,.tt-suggestion:hover {
  color: #fff;
  background-color: #0097cf;

}
</style>
@endsection
@section('content')
<div class="l-main-container">
<div class="b-slidercontainer b-slider b-slider--thumb">
  <div class=" j-fullscreenslider j-arr-nexttobullets">
    <ul>
      @foreach($slide as $slider)
      <li data-transition="slotfade-vertical" data-slotamount="7" >
        <div class="tp-bannertimer">
        </div>
        <img data-retina src="{{$slider->file_foto}}">
        <div class="caption lft caption-left"  data-x="left" data-y="150" data-hoffset="60" data-speed="700" data-start="2000">
          <div class="b-slider-lg-info-l__item-title f-slider-lg-info-l__item-title b-slider-lg-info-l__item-title-tertiary b-bg-slider-lg-info-l__item-title">
            <!-- <h2 class="f-primary-l">frexy psd
            </h2> -->
            <h1 class="f-primary-b">{{$slider->nama_foto}}
            </h1>
          </div>
        </div>
        <div class="caption lfl caption-left"  data-x="left" data-y="270" data-speed="1000" data-start="2600">
          <div class="b-slider-lg-info-l__item-link f-slider-lg-info-l__item-link">
            <a href="{{url('/tour-detail/'.$slider->idPaket)}}" class="b-slider-lg-info-l__item-anchor f-slider-lg-info-l__item-anchor f-primary-b">Order Now!
            </a>
            <span class="b-slider-lg-info-l__item-link-after">
              <i class="fa fa-chevron-right">
              </i>
            </span>
          </div>
        </div>
      </li>
      @endforeach
      <!-- <li data-transition="" data-slotamount="10" >
        <div class="tp-bannertimer">
        </div>
        <img data-retina src="assets/img/slider/slider-section-2.jpg">
        <div class="caption lft caption-left"  data-x="left" data-y="100" data-hoffset="60" data-speed="700" data-start="2000">
          <div class="b-slider-lg-info-l__item-title f-slider-lg-info-l__item-title b-slider-lg-info-l__item-title-tertiary">
            <h2 class="f-primary-l">frexy psd template
            </h2>
            <h1 class="f-primary-b">travel version
            </h1>
            <p>Vivamus blandit velit ante, vel ullamcorper ex viverra et. Phasellus tristique,
              <br /> tellus vel molestie suscipit
            </p>
          </div>
        </div>
        <div class="caption lfl caption-left"  data-x="left" data-y="270" data-speed="1000" data-start="2600">
          <div class="b-slider-lg-info-l__item-link f-slider-lg-info-l__item-link">
            <a href="#" class="b-slider-lg-info-l__item-anchor f-slider-lg-info-l__item-anchor f-primary-b">multi-purposes psd template
            </a>
            <span class="b-slider-lg-info-l__item-link-after">
              <i class="fa fa-chevron-right">
              </i>
            </span>
          </div>
        </div>
      </li>
      <li data-transition="" data-slotamount="10" >
        <div class="tp-bannertimer">
        </div>
        <img data-retina src="assets/img/slider/slider-section-3.jpg">
        <div class="caption lft caption-left"  data-x="left" data-y="100" data-hoffset="60" data-speed="700" data-start="2000">
          <div class="b-slider-lg-info-l__item-title f-slider-lg-info-l__item-title b-slider-lg-info-l__item-title-tertiary">
            <h2 class="f-primary-l">frexy travel template
            </h2>
            <h1 class="f-primary-b">find and booking hotels
            </h1>
            <p>Vivamus blandit velit ante, vel ullamcorper ex viverra et. Phasellus tristique,
              <br /> tellus vel molestie suscipit
            </p>
          </div>
        </div>
        <div class="caption lfl caption-left"  data-x="left" data-y="270" data-speed="1000" data-start="2600">
          <div class="b-slider-lg-info-l__item-link f-slider-lg-info-l__item-link">
            <a href="#" class="b-slider-lg-info-l__item-anchor f-slider-lg-info-l__item-anchor f-primary-b">discover frexy travel now
            </a>
            <span class="b-slider-lg-info-l__item-link-after">
              <i class="fa fa-chevron-right">
              </i>
            </span>
          </div>
        </div>
      </li> -->
    </ul>
  </div>
</div>
<section class="b-infoblock">
  <div class="container">
    <div class="b-search-map">
      <div class="j-tabs b-tabs-reset b-search-map-tabs">
        <div class="b-search-map_header">
          <div class="b-search-map__title f-search-map__title f-primary-b">Quick search
          </div>
          <ul class="b-search-map__tabs-anchor f-search-map__tabs-anchor">
            <li>
              <a class="f-primary-b" href="#tabs-1">TOURS
              </a>
            </li>
          </ul>
        </div>
        <div id="tabs-1">
          <div class="b-search-map__wrap">
            <form method="get" action="/" class="b-form f-form b-form-inline f-form-inline">
              <div class="b-search-map__name f-search-map__name">
                <span class="b-search-map__name_hight f-search-map__name_hight f-primary-b">book
                </span>
                <br/>
                your tours
              </div>
              <div class="b-search-map__fields f-search-map__fields">
                <div class="b-form-group">
                  <label class="f-primary-b" for="tourLocation">Tour location
                  </label>
                  <div id="the-basics">
                    <input class="typeahead b-form-control" id="tourLocation" type="text" placeholder="Pilih Lokasi">
                  </div>
                  <!-- <input type="text" class="b-form-control" id="tourLocation"> -->
                </div>
                <!-- <div class="b-form-group">
                  <label class="f-primary-b" for="startDate">Start date
                  </label>
                  <div class="b-form-control__icon-wrap">
                    <input type="text" class="b-form-control datepicker" id="startDate">
                    <i class="fa fa-calendar b-form-control__icon f-form-control__icon">
                    </i>
                  </div>
                </div> -->
                <div class="b-form-group">
                  <label class="f-primary-b" for="guests">guests
                  </label>
                  <input type="text" class="b-form-control" id="guests">
                </div>
                <div class="b-form-group">
                  <label class="f-primary-b">find tour
                  </label>
                  <button class="b-search-map__submit b-btn f-btn b-btn-light f-btn-light f-primary-b">search
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="b-infoblock b-diagonal-line-bg-light">
  <div class="container">
    <div class="b-carousel-secondary f-carousel-secondary b-some-examples-tertiary f-some-examples-tertiary b-carousel-reset">
      <div class="b-carousel-title f-carousel-title f-primary-b">special offers
      </div>
      <div class="b-some-examples f-some-examples j-carousel-secondary">
        @foreach($paket as $pakets)
        <div class="b-some-examples__item f-some-examples__item">
          <div class="b-some-examples__item_img view view-sixth">
            <img data-retina="" src="{{$pakets->file_foto}}" alt="{{$pakets->nama_foto}}">
            <div class="b-item-hover-action f-center mask">
              <div class="b-item-hover-action__inner">
                <div class="b-item-hover-action__inner-btn_group">
                  <a href="{{url('/tour-detail/'.$pakets->idPaket)}}" class="b-btn f-btn b-btn-light f-btn-light info">
                    <i class="fa fa-link">
                    </i>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="b-some-examples__item_info">
            <div class="b-some-examples__item_info_level b-some-examples__item_name f-some-examples__item_name f-primary-b">
              <a href="{{url('/tour-detail/'.$pakets->idPaket)}}">{{$pakets->nmPaket}}
              </a>
            </div>
            <div class="b-some-examples__item_info_level b-some-examples__item_double_info f-some-examples__item_double_info clearfix">
              <div class="b-stars-group f-stars-group">
                <i class="fa fa-star is-active-stars">
                </i>
                <i class="fa fa-star is-active-stars">
                </i>
                <i class="fa fa-star is-active-stars">
                </i>
                <i class="fa fa-star is-active-stars">
                </i>
                <i class="fa fa-star">
                </i>
              </div>
            </div>
            <div class="b-some-examples__item_info_level b-some-examples__item_description f-some-examples__item_description">
              {{$pakets->desPaket}}
            </div>
          </div>
          <div class="b-some-examples__item_action">
            <div class="b-right">
              <a href="{{url('/tour-detail/'.$pakets->idPaket)}}" class="b-btn f-btn b-btn-sm f-btn-sm b-btn-default f-primary-b">book now
              </a>
            </div>
            <div class="b-remaining b-some-examples__item_total f-some-examples__item_total f-primary-b">From IDR {{number_format($pakets->hrgPaket, 2, ',', '.')}}
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</section>
<section class="b-infoblock b-diagonal-line-bg-light">
  <div class="container">
    <div class="b-carousel-secondary f-carousel-secondary b-carousel-reset">
      <div class="b-carousel-title f-carousel-title f-primary-b">POPULAR TRAVELS
      </div>
      <div class="b-some-examples f-some-examples f-some-examples-quaternary j-carousel-secondary">
        @foreach($paket as $pakets)
        <div class="b-some-examples__item f-some-examples__item">
          <div class="b-some-examples__item_img view view-sixth">
            <img data-retina="" src="{{$pakets->file_foto}}" alt="{{$pakets->nama_foto}}">
            <div class="b-item-hover-action f-center mask">
              <div class="b-item-hover-action__inner">
                <div class="b-item-hover-action__inner-btn_group">
                  <a href="{{url('/tour-detail/'.$pakets->idPaket)}}" class="b-btn f-btn b-btn-light f-btn-light info">
                    <i class="fa fa-link">
                    </i>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="b-some-examples__item_info">
            <div class="b-some-examples__item_info_level b-some-examples__item_name f-some-examples__item_name f-primary-b">
              <a href="{{url('/tour-detail/'.$pakets->idPaket)}}">{{$pakets->nmPaket}}
              </a>
            </div>
            <div class="b-some-examples__item_info_level b-some-examples__item_double_info f-some-examples__item_double_info clearfix">
              <div class="b-right f-selection f-primary f-size-default">
                23 reviews
              </div>
              <div class="b-stars-group f-stars-group">
                <i class="fa fa-star is-active-stars">
                </i>
                <i class="fa fa-star is-active-stars">
                </i>
                <i class="fa fa-star is-active-stars">
                </i>
                <i class="fa fa-star is-active-stars">
                </i>
                <i class="fa fa-star">
                </i>
              </div>
            </div>
            <div class="b-some-examples__item_info_level b-some-examples__item_description f-some-examples__item_description">
              {{$pakets->desPaket}}
            </div>
          </div>
          <div class="b-some-examples__item_action">
            <div class="b-right">
              <a href="{{url('/tour-detail/'.$pakets->idPaket)}}" class="b-btn f-btn b-btn-sm f-btn-sm b-btn-default f-primary-b">book now
              </a>
            </div>
            <div class="b-remaining b-some-examples__item_total f-some-examples__item_total f-primary-b">From IDR {{number_format($pakets->hrgPaket, 2, ',', '.')}}
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</section>
<!-- <section class="b-about-container f-about-container b-bg-street b-about-container__with-img-l b-about-container--high">
  <div class="container">
    <div class="b-about-container__img">
      <img data-retina src="assets/img/animation-data/about-img2.png" alt="">
    </div>
    <div class="b-about-container__inner b-slider-reset b-slider-about">
      <div class="j-slider-primary">
        <div>
          <div class="b-about-container__title f-about-container__title f-primary-b">tell us your travel stories
          </div>
          <div class="b-about-container__title_second f-about-container__title_second f-primary-l">Lorem ipsum dolor sit amet, consectetur adiet
          </div>
          <div class="b-about-container__text f-about-container__text f-primary-l">
            Maecenas eros tellus, vulputate in felis eu, tristique lobortis nisl. Nullam ornare, justo sit amet cursus auctor, lorem lacus laoreet ante, nec lacinia diam urna ut justo. Pellentesque blandit iaculis justo, ut imperdiet metus eleifend nec. Aenean a vestibulum risus. Nulla vitae ultricies velit, et pulvinar mauris. Donec mattis sagittis enim. Vivamus ac sapien placerat nisi elementum Curabitur blandit tempus porttitor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          </div>
        </div>
        <div>
          <div class="b-about-container__title f-about-container__title f-primary-b">tell us your travel stories
          </div>
          <div class="b-about-container__title_second f-about-container__title_second f-primary-l">Lorem ipsum dolor sit amet, consectetur adiet
          </div>
          <div class="b-about-container__text f-about-container__text f-primary-l">
            Maecenas eros tellus, vulputate in felis eu, tristique lobortis nisl. Nullam ornare, justo sit amet cursus auctor, lorem lacus laoreet ante, nec lacinia diam urna ut justo. Pellentesque blandit iaculis justo, ut imperdiet metus eleifend nec. Aenean a vestibulum risus. Nulla vitae ultricies velit, et pulvinar mauris. Donec mattis sagittis enim. Vivamus ac sapien placerat nisi elementum Curabitur blandit tempus porttitor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          </div>
        </div>
        <div>
          <div class="b-about-container__title f-about-container__title f-primary-b">tell us your travel stories
          </div>
          <div class="b-about-container__title_second f-about-container__title_second f-primary-l">Lorem ipsum dolor sit amet, consectetur adiet
          </div>
          <div class="b-about-container__text f-about-container__text f-primary-l">
            Maecenas eros tellus, vulputate in felis eu, tristique lobortis nisl. Nullam ornare, justo sit amet cursus auctor, lorem lacus laoreet ante, nec lacinia diam urna ut justo. Pellentesque blandit iaculis justo, ut imperdiet metus eleifend nec. Aenean a vestibulum risus. Nulla vitae ultricies velit, et pulvinar mauris. Donec mattis sagittis enim. Vivamus ac sapien placerat nisi elementum Curabitur blandit tempus porttitor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          </div>
        </div>
      </div>
    </div>
  </div>
</section> -->
</div>
@endsection
@section('javascript')
<script type="text/javascript">
$(document).ready(function() {
  var substringMatcher = function(strs) {
    return function findMatches(q, cb) {
      var matches, substringRegex;

      // an array that will be populated with substring matches
      matches = [];

      // regex used to determine if a string contains the substring `q`
      substrRegex = new RegExp(q, 'i');

      // iterate through the pool of strings and for any string that
      // contains the substring `q`, add it to the `matches` array
      $.each(strs, function(i, str) {
        if (substrRegex.test(str)) {
          matches.push(str);
        }
      });

      cb(matches);
    };
  };

  var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
    'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
    'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
    'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
    'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
    'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
    'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
    'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
    'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
  ];

  $('#the-basics .typeahead').typeahead({
    hint: true,
    highlight: true,
    minLength: 1
  },
  {
    name: 'states',
    source: substringMatcher(states)
  });
});
</script>
@endsection