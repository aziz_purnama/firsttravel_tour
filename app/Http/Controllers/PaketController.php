<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Mail;
use App;
use PDF;
use Illuminate\Pagination\Paginator;

class PaketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Paket  $paket
     * @return \Illuminate\Http\Response
     */
    public function show(Paket $paket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Paket  $paket
     * @return \Illuminate\Http\Response
     */
    public function edit(Paket $paket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Paket  $paket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paket $paket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Paket  $paket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paket $paket)
    {
        //
    }

    public function generateRandomString($length) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
     }

    public function internasional()
    {
        $paket      = DB::table('tbl_paket')
                        //->join('tbl_fotopaket', 'tbl_paket.idPaket', '=', 'tbl_fotopaket.id_paket')
                        ->where('kategoriPaket','=','Internasional')
                        ->paginate(9);
        return view('tour-list.tour-list-internasional')->with(compact('paket'));
    }

    public function domestik()
    {
        $paket      = DB::table('tbl_paket')
                        //->join('tbl_fotopaket', 'tbl_paket.idPaket', '=', 'tbl_fotopaket.id_paket')
                        ->where('kategoriPaket','=','Domestik')
                        ->paginate(9);
        return view('tour-list.tour-list-domestik')->with(compact('paket'));
    }

    public function tourDetail($id)
    {
        $paket      = DB::table('tbl_paket')
                        ->join('tbl_itinerary', 'tbl_paket.idPaket', '=', 'tbl_itinerary.idPaket')
                        ->where('tbl_paket.idPaket','=',$id)
                        ->first();
        $foto      = DB::table('tbl_paket')
                        ->join('tbl_fotopaket', 'tbl_paket.idPaket', '=', 'tbl_fotopaket.id_paket')
                        ->where('tbl_paket.idPaket','=',$id)
                        ->get();
        return view('tour-detail.tour-detail')->with(compact('paket','foto'));
    }

    public function submit($id,Request $request)
    {
        $paket      = DB::table('tbl_paket')
                        ->join('tbl_fotopaket', 'tbl_paket.idPaket', '=', 'tbl_fotopaket.id_paket')
                        ->where('tbl_paket.idPaket','=',$id)
                        ->first();
        return view('order.order')->with(compact('paket','request'));
    }
    public function save(Request $request)
    {
        $now        = Carbon::now();
       
        $invoice    = 'INV'.$this->generateRandomString(6).$now->year.$now->month.$now->day;
        DB::table('tbl_transaksi')->insert([
            'noTrans' => $invoice, 
            'tglTrans' => $now, 
            'idPaket' => $request->id,
            'nmPaket' => $request->nama_paket,
            'jmlPax' => $request->total_pesanan,
            'nmTraveler' => $request->nama,
            'dobTraveler' => $request->tgl_lahir,
            'jnsIdTraveler' => $request->jns_id,
            'IdTraveler' => $request->no_id,
            'passTraveler' => $request->passport,
            'emailTraveler' => $request->email,
            'tlpTraveler' => $request->no_tlp,
            'hrgPaketSatuan' => $request->harga_satuan,
            'hrgTotal' => $request->harga_total,
            'jnsPembyaran' => $request->jns_pembayaran,
            'statusPembayaran' => 0,
            'createDate' => $now
        ]);

        $paket      = DB::table('tbl_transaksi')
                        ->orderBy('idTrans','desc')
                        ->first();
        
        foreach ($request->get('namaDetail') as $key => $value) {
            DB::table('tbl_transaksidetail')->insert([
                'idTrans' => $paket->idTrans, 
                'nmTravelerDet' => $request->namaDetail[$key],
                'dobTravelerDet' => $request->tglLahir[$key],
                'createDate' => $now
            ]);
        }
        
        Mail::send('template/email-template', ['paket' => $paket], function ($m) use($request){
            $m->to($request->email, $request->nama)->subject('Invoice');
        });

        return view('invoice.invoice')->with(compact('paket'));
    }

    public function pdf($id){
        $paket      = DB::table('tbl_transaksi')
                        ->where('noTrans','=',$id)
                        ->orderBy('idTrans','desc')
                        ->first();
        $pdf = PDF::loadView('template.pdf-template',array('paket' => $paket));
        return $pdf->download('invoice.pdf');
    }
}
