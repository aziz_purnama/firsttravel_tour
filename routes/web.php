<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/tour-list-internasional', function () {
//     return view('tour-list.tour-list-internasional');
// });
Route::get('/tour-list-internasional','PaketController@internasional');
Route::get('/tour-list-domestik','PaketController@domestik');
Route::get('/tour-detail/{id}','PaketController@tourDetail');
Route::get('/','HomeController@index');
Route::post('/tour-detail/submit/{id}', 'PaketController@submit');
Route::post('/tour-detail/save', 'PaketController@save');
Route::get('/print/{id}', 'PaketController@pdf');
Route::get('/konfirmasi', 'KonfirmasiController@index');
Route::get('/konfirmasi/{id}', 'KonfirmasiController@store');
//Route::get('/outstanding', 'OutstandingController@index');
// Route::get('/invoice', function () {
//     return view('invoice.invoice');
// });

// Route::get('/konfirmasi', function () {
//     return view('konfirmasi.konfirmasi');
// });
// Route::get('test', function()
// {
//     dd(Config::get('mail'));
// });

// Route::get('/tour-detail', function () {
//     return view('tour-detail.tour-detail');
// });
// Route::get('/home', function () {
//     return view('home.home');
// });