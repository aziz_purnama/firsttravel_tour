<?php
// Apabila user belum login
if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
  echo "<link href=\"css/style_login.css\" rel=\"stylesheet\" type=\"text/css\" />
        <div id=\"login\"><h1 class=\"fail\">Untuk mengakses modul, Anda harus login dulu.</h1>
        <p class=\"fail\"><a href=\"index.php\">LOGIN</a></p></div>";
}
// Apabila user sudah login dengan benar, maka terbentuklah session

else{
  include "config/koneksi.php";
  //include "../config/library.php";

  // Home (Beranda)
  if ($_GET['mod']=='home'){
    if ($_SESSION['leveluser']=='1'){
      include "modul/mod_home/home.php";
    }
 }

//Paket
elseif ($_GET['mod']=='paket'){
   if ($_SESSION['leveluser']=='1'){
      include "modul/mod_paket/mod_paket.php";
   }
  }
  elseif ($_GET['mod']=='foto-paket'){
   if ($_SESSION['leveluser']=='1'){
      include "modul/mod_foto_paket/mod_foto_paket.php";
   }
  }

//Itinerary
    elseif ($_GET['mod']=='itinerary'){
   if ($_SESSION['leveluser']=='1'){
      include "modul/mod_itinerary/mod_itinerary.php";
   }
  }

  //Transaksi
    elseif ($_GET['mod']=='transaksi'){
   if ($_SESSION['leveluser']=='1'){
      include "modul/mod_transaksi/mod_transaksi.php";
   }
  }

   //Setting
    elseif ($_GET['mod']=='user'){
   if ($_SESSION['leveluser']=='1'){
      include "modul/mod_user/mod_user.php";
   }
  }

    elseif ($_GET['mod']=='tambah-user'){
   if ($_SESSION['leveluser']=='1'){
      include "modul/mod_tmbuser/mod_tmbuser.php";
   }
  }

  // Apabila modul tidak ditemukan
  else{
    echo "<p>Modul tidak ada.</p>";
  }
}
?>

