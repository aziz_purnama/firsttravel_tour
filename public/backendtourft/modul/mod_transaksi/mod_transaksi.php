<link rel="stylesheet" href="assets/vendor/summernote/summernote.css" />
<link rel="stylesheet" href="assets/vendor/summernote/summernote-bs3.css" />
<script src="assets/vendor/summernote/summernote.js"></script>
<style type="text/css">
	.container{
    margin-top:20px;
	}
	.image-preview-input {
	    position: relative;
		overflow: hidden;
		margin: 0px;    
	    color: #333;
	    background-color: #fff;
	    border-color: #ccc;    
	}
	.image-preview-input input[type=file] {
		position: absolute;
		top: 0;
		right: 0;
		margin: 0;
		padding: 0;
		font-size: 20px;
		cursor: pointer;
		opacity: 0;
		filter: alpha(opacity=0);
	}
	.image-preview-input-title {
	    margin-left:2px;
	}
</style>
<script language="javascript">
    function hanyaAngka(e, decimal) { 
    var key;
    var keychar;
     if (window.event) {
         key = window.event.keyCode;
     } else
     if (e) {
         key = e.which;
     } else return true;
    
    keychar = String.fromCharCode(key);
    if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
        return true;
    } else 
    if ((("0123456789").indexOf(keychar) > -1)) {
        return true; 
    } else 
    if (decimal && (keychar == ".")) {
        return true; 
    } else return false; 
    }
</script>

<?php 
$aksi = "modul/mod_transaksi/action_transaksi.php";
// mengatasi variabel yang belum di definisikan (notice undefined index)
$act = isset($_GET['act']) ? $_GET['act'] : ''; 
  switch($act){
  default:    
 ?>
 <div class="tab-content">
<div id="jamaah" class="tab-pane fade in active">
<div class="row">
	<div class="col-lg-12">
		<section class="panel panel-primary">
			<header class="panel-heading">
				<h2 class="panel-title">List Transaksi</h2>
			</header>
			<div class="panel-body">
			<div class="row">
					
				</div>
				<table class="table table-bordered table-striped table-hover mb-none" id="dataTable-trans" >
					<thead>
					  <tr>
						<th>No Transaksi</th>
						<th>Tanggal Transaksi</th>
						<th>Nama Paket</th>
						<th>Jumlah Pax</th>
						<th>Nama Traveler</th>
						<th>Status Pembayaran</th>
						<th>Actions</th>
					  </tr>
					</thead>
				</table>
			</div>
		</section>
	</div>
</div>
</div>

</div>
<script>
	var dataTable;
	$(document).ready(function() {
		dataTable = $('#dataTable-trans').DataTable( {
			"processing": true,
			"serverSide": true,
			"ajax": "modul/mod_transaksi/json_transaksi.php"
		});
		
	});

	function deleteTrans(idTrans){
		$.ajax({
			type: "POST",
			url: "modul/mod_transaksi/delete_transaksi.php",
			dataType: 'json',
			data: {id:idTrans},
			beforeSend: function(){
     			alert("Apakah ingin menghapus data ini?");
  			},
			success: function(data) {
				alert("Data Berhasil dihapus");
				dataTable.ajax.reload();
			}
		});
	}
</script>
<?php 
break;
case "detail":
$id=$_GET['id'];
$query="select * from tbl_transaksi where idTrans=$id";
$proses=mysqli_query($konek,$query);
$row=mysqli_fetch_array($proses);
if($row['statusPembayaran'] == "0"){
	$status = "Menunggu Konfirmasi";
}else if($row['statusPembayaran'] == "1"){
	$status = "Sudah di Validasi";
}else if($row['statusPembayaran'] == "2"){
	$status = "Sudah Konfirmasi";
}else{
	$status = "-";
}
?>


<ul class="nav nav-tabs">
	<li class="active"><a data-toggle="tab" href="#jamaah">Header Transaksi</a></li>
	<li><a data-toggle="tab" id="tab-agent" href="#agent">Detail Transaksi</a></li>
</ul>
<div class="tab-content">
	<div id="jamaah" class="tab-pane fade in active">
		<div class="row">
			<div class="col-lg-12">
				<section class="panel panel-primary">
					<div class="panel-body">
						<form class="form-horizontal form-bordered" id="form-trans" enctype="multipart/form-data">
							<input type="hidden" name="idTrans" id="idTrans" value="<?php echo $row['idTrans']; ?>" required>
							<input type="hidden" name="modul" id="modul" value="transaksi" required>
							<div class="form-group">
								<label class="col-md-3 control-label">No Transaksi</label>
								<div class="col-sm-6">
									<input type="text" name="no_trans" class="form-control" value="<?php echo $row['noTrans']; ?>" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Tanggal Transaksi</label>
								<div class="col-sm-6">
									<input type="text" name="tgl_trans" class="form-control" value="<?php echo $row['tglTrans']; ?>" readonly>
								</div>
							</div>
							<span class="center help-block">Data Diri Traveler</span>
							<div class="form-group">
								<label class="col-md-3 control-label">Nama Traveler</label>
								<div class="col-sm-6">
									<input type="text" name="nama_trav" class="form-control" value="<?php echo $row['nmTraveler']; ?>" readonly>
									
								</div>	
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Tanggal lahir Traveler</label>
								<div class="col-sm-6">
									<input type="text" name="tgl_trav" class="form-control" value="<?php echo $row['dobTraveler']; ?>" readonly>  
									
								</div>	
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Email Traveler</label>
								<div class="col-sm-6">
									<input type="text" name="email_trav" class="form-control" value="<?php echo $row['emailTraveler']; ?>" readonly>
									
								</div>	
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Telepon Traveler</label>
								<div class="col-sm-6">
									<input type="text" name="tlp_trav" class="form-control" value="<?php echo $row['tlpTraveler']; ?>" readonly>
									
								</div>	
							</div>
							<span class="center help-block">ID Traveler</span>
							<div class="form-group">
								<label class="col-md-3 control-label">Jenis ID</label>
								<div class="col-sm-6">
									<input type="text" name="jnsid_trav" class="form-control" value="<?php echo $row['jnsIdTraveler']; ?>" readonly>
									
								</div>	
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">No ID</label>
								<div class="col-sm-6">
									<input type="text" name="noid_trav" class="form-control" value="<?php echo $row['idTraveler']; ?>" readonly>
									
								</div>	
							</div>
							<span class="center help-block">Passpor Traveler</span>
							<div class="form-group">
								<label class="col-md-3 control-label">No Passpor</label>
								<div class="col-sm-6">
									<input type="text" name="passpor_trav" class="form-control" value="<?php echo $row['passTraveler']; ?>" readonly>
									
								</div>	
							</div>
							<span class="center help-block">Paket yang dipesan</span>
							<div class="form-group">
								<label class="col-md-3 control-label">Paket</label>
								<div class="col-sm-6">
									<input type="text" name="nama_paket" class="form-control" value="<?php echo $row['nmPaket']; ?>" readonly>
									
								</div>	
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Pax</label>
								<div class="col-sm-6">
									<input type="text" name="jml_pax" class="form-control" value="<?php echo $row['jmlPax']; ?>" readonly>
									
								</div>	
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Harga Paket (satuan)</label>
								<div class="col-sm-6">
									<input type="text" name="hrg_trav" class="form-control" value="<?php echo $row['hrgPaketSatuan']; ?>" readonly>
									
								</div>	
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Harga Paket (Total)</label>
								<div class="col-sm-6">
									<input type="text" name="tot_trav" class="form-control" value="<?php echo $row['hrgTotal']; ?>" readonly>
									
								</div>	
							</div>
							<span class="center help-block">Metode Pembayaran</span>
							<div class="form-group">
								<label class="col-md-3 control-label">Jenis Pembayaran</label>
								<div class="col-sm-6">
									<input type="text" name="jns_trav" class="form-control" value="<?php echo $row['jnsPembyaran']; ?>" readonly> 
									
								</div>	
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Status Pembayaran</label>
								<div class="col-sm-6">
									<input type="text" name="stat_trav" class="form-control" value="<?php echo $status; ?>" readonly>
									
								</div>	
							</div>
							<?php
								if($row['statusPembayaran'] == "1"){
							?>
							<div class="form-group">
								<div class="col-sm-6">
									<a class='btn btn-danger' href="dashboard.php?mod=transaksi" > Back</a>
								</div>
							</div>
							<?php
								}else{
							?>
							<div class="form-group">
								<div class="col-sm-6">
									<a class='btn btn-danger' href="dashboard.php?mod=transaksi" > Back</a>
									<button class="btn btn-primary" type="button" id="done">Validasi</button>
								</div>
							</div>
							<?php
								}
							?>
						</form>
					</div>
				</section>
			</div>
		</div>
	</div>
	<div id="agent" class="tab-pane fade">
		<div class="row">
			<div class="col-lg-12">
				<section class="panel panel-primary">
				<?php 
					$i=1;
					$query="select nmTravelerDet, dobTravelerDet, b.tglTrans from tbl_transaksidetail as a inner join tbl_transaksi as b on a.idTrans = b.idTrans where a.idTrans=$id";
					$proses=mysqli_query($konek,$query);
					while($rowe=mysqli_fetch_array($proses)){

				?>
					<header class="panel-heading">
						<div class="panel-actions">
							<a href="#" class="fa fa-caret-down"></a>
							<a href="#" class="fa fa-times"></a>
						</div>
						<h2 class="panel-title">Data Detail Traveler ke <?php echo $i; ?> </h2>
					</header>
					<div class="panel-body">
						<div class="form-group">
							<label class="col-md-3 control-label">Nama Traveler</label>
							<div class="col-sm-6">
								<input type="text" name="no_trans" class="form-control" value="<?php echo $rowe['nmTravelerDet']; ?>" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Tanggal Lahir Traveler</label>
							<div class="col-sm-6">
								<input type="text" name="tgl_trans" class="form-control" value="<?php echo $rowe['dobTravelerDet']; ?>" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Tanggal Transaksi</label>
							<div class="col-sm-6">
								<input type="text" name="nama_trav" class="form-control" value="<?php echo $rowe['tglTrans']; ?>" readonly>
								
							</div>	
						</div>
					</div>
					<?php $i++; } ?>
				</section>
			</div>
		</div>
	</div>
</div>

<!--Modal Loading -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      	<div class="modal-content">
        	<div class="modal-header">
          		<h4 class="modal-title">Validasi Sukses!</h4>
        	</div>
	        <div class="modal-body">
	          	<p>Data anda telah tervalidasi! Terimakasih.</p>	
	        </div>
      	</div>
    </div>
</div>
<script type="text/javascript">

$(document).ready(function() {
	//Modal Loading
	var waitingDialog = waitingDialog || (function ($) {
	    'use strict';

		// Creating modal dialog's DOM
		var $dialog = $(
			'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
			'<div class="modal-dialog modal-m">' +
			'<div class="modal-content">' +
				'<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
				'<div class="modal-body">' +
					'<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
				'</div>' +
			'</div></div></div>');

		return {
			/**
			 * Opens our dialog
			 * @param message Custom message
			 * @param options Custom options:
			 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
			 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
			 */
			show: function (message, options) {
				// Assigning defaults
				if (typeof options === 'undefined') {
					options = {};
				}
				if (typeof message === 'undefined') {
					message = 'Loading';
				}
				var settings = $.extend({
					dialogSize: 'm',
					progressType: '',
					onHide: null // This callback runs after the dialog was hidden
				}, options);

				// Configuring dialog
				$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
				$dialog.find('.progress-bar').attr('class', 'progress-bar');
				if (settings.progressType) {
					$dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
				}
				$dialog.find('h3').text(message);
				// Adding callbacks
				if (typeof settings.onHide === 'function') {
					$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
						settings.onHide.call($dialog);
					});
				}
				// Opening dialog
				$dialog.modal();
			},
			/**
			 * Closes dialog
			 */
			hide: function () {
				$dialog.modal('hide');
			}
		};

	})(jQuery);

//Ketika tombol Validasi Di klik
	$('#done').click(function(){   
		var id = $('#idTrans').val();

	$.ajax({
			type: "POST",
			dataType: "json",
			url:"modul/mod_transaksi/action_transaksi.php",
			data: {id:id},
			beforeSend: function() {
				// setting a timeout
				console.log("error");
				waitingDialog.show();
			},
			success: function(result) {	
				waitingDialog.hide();
				$("#myModal").modal("show");
				$("#done").hide();
			}

			});
	});
	
	$('#myModal').click(function(){ 
		window.location="dashboard.php?mod=transaksi";
	});

});
</script>

<?php
}
?>
	



