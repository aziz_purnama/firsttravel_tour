<?php
$aksi = "modul/mod_transaksi/mod_transaksi.php";
require( '../../config/koneksi.php' );

// storing  request (ie, get/post) global array to a variable  
$requestData= $_REQUEST;

$columns = array( 
// datatable column index  => database column name
	0 => 'idTrans', 
	1 => 'tglTrans',
	2 => 'noTrans',
	3 => 'nmPaket', 
	4 => 'jmlPax',
	5 => 'nmTraveler'
);

// getting total number records without any search
$sql = "select * from tbl_transaksi";
$query=mysqli_query($konek, $sql) or die("blacklist_1");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.


$sql = "select * from tbl_transaksi where 1=1 ";
// getting records as per search parameters
if( !empty($requestData['search']['value']) ){
	$sql.=" and tglTrans like '%".$requestData['search']['value']."%' or nmTraveler like '%".$requestData['search']['value']."%'";
	$sql.=" or noTrans like '%".$requestData['search']['value']."%' or nmPaket like '%".$requestData['search']['value']."%'";
}
// if( !empty($requestData['columns'][2]['search']['value']) ){
	// $sql.="WHERE noreg_jamaah = '".$requestData['columns'][2]['search']['value']."%' ";
// }

$query=mysqli_query($konek, $sql) or die("blacklist_2");
$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

$sql.=" ORDER BY idTrans DESC LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	
//$sql.=" GROUP BY ". $columns[$requestData['order'][0]['column']]." ORDER BY ". $columns[$requestData['order'][0]['column']]."  ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";  // adding length

$query=mysqli_query($konek, $sql) or die("error");


$data = array();
//$idsrh = $row["srh_id"];
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
	$nestedData=array();
	
	if($row['statusPembayaran'] == "0"){
		$status = "Menunggu Konfirmasi";
	}else if($row['statusPembayaran'] == "1"){
		$status = "Sudah di Validasi";
	}else if($row['statusPembayaran'] == "2"){
		$status = "Sudah Konfirmasi";
	}else{
		$status = "-";
	}
	
	$nestedData[] = $row["noTrans"];
	$nestedData[] = $row["tglTrans"];
	$nestedData[] = $row["nmPaket"];
	$nestedData[] = $row["jmlPax"];
	$nestedData[] = $row["nmTraveler"];
	$nestedData[] = $status;
	

	$nestedData[] = '<a href="?mod=transaksi&act=detail&id='.$row["idTrans"].'"><i class="fa fa-edit"> Detail</i></a>';
	
	$data[] = $nestedData;
	
}



$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);


echo json_encode($json_data);  // send data as json format

?>