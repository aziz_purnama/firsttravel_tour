<link rel="stylesheet" href="assets/vendor/summernote/summernote.css" />
<link rel="stylesheet" href="assets/vendor/summernote/summernote-bs3.css" />
<script src="assets/vendor/summernote/summernote.js"></script>


<?php 
$aksi = "modul/mod_itinerary/action_itinerary.php";
// mengatasi variabel yang belum di definisikan (notice undefined index)
$act = isset($_GET['act']) ? $_GET['act'] : ''; 
  switch($act){
  default:    
 ?>
 <div class="tab-content">
<div id="jamaah" class="tab-pane fade in active">
<div class="row">
	<div class="col-lg-12">
		<section class="panel panel-primary">
			<header class="panel-heading">
				<div class="panel-actions">
					<div class="mb-md">
						<button id="add_page" class="btn btn-primary" onclick="location.href = '?mod=itinerary&act=insert';">Add <i class="fa fa-plus"></i></button>
					</div>
				</div>
				<h2 class="panel-title">Itinerary</h2>

			</header>
			<div class="panel-body">
			<div class="row">
					
				</div>
				<table class="table table-bordered table-striped table-hover mb-none" id="dataTable-itinerary" >
					<thead>
					  <tr>
						<th>Nama Paket</th>
						<th>Itinerary</th>
						<th>Actions</th>
					  </tr>
					</thead>
				</table>
			</div>
		</section>
	</div>
</div>
</div>

</div>
<script>
	var dataTable;
	$(document).ready(function() {
		dataTable = $('#dataTable-itinerary').DataTable( {
			"processing": true,
			"serverSide": true,
			"ajax": "modul/mod_itinerary/json_itinerary.php"
		} );
		
	});

	function deleteItinerary(id){
		$.ajax({
			type: "POST",
			url: "modul/mod_itinerary/delete_itinerary.php",
			dataType: 'json',
			data: {id:id},
			beforeSend: function(){
     			alert("Apakah ingin menghapus data ini?");
  			},
			success: function(data) {
				alert("Data Berhasil dihapus");
				dataTable.ajax.reload();
			}
		});
	}
</script>
 <?php
  break;
  case "insert":
?>


<div class="row">
	<div class="col-lg-12">
		<section class="panel panel-primary">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a>
					<a href="#" class="fa fa-times"></a>
				</div>
				<h2 class="panel-title">Tambah Itinerary</h2>
			</header>
			<div class="panel-body">
				<form class="form-horizontal form-bordered" id="form-itinerary" action="<?php echo $aksi;?>?mod=itinerary&act=insert"  method="post">
					<input type="hidden" name="namauser" value="<?php echo $_SESSION['namauser']; ?>" required>
					<div class="form-group">
						<label class="col-md-3 control-label">Paket</label>
						<div class="col-sm-6">
						<?php
						$query="select * from tbl_paket";
						$proses=mysqli_query($konek,$query);
						
						?>
							<select name="nama_paket" class="form-control">
								<option>---Pilih Paket---</option>
								<?php
									while ($row=mysqli_fetch_array($proses)) {
								?>
								<option value="<?php echo $row['idPaket'];?>"><?php echo $row['nmPaket'];?></option>
								<?php
									}
								?>
								
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Itinerary Paket</label>
						<div class="col-md-9">
							<textarea class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }' name="itinerary_paket">
							</textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-6">
							<a class='btn btn-danger' href="dashboard.php?mod=itinerary" > Back</a>
							<button class="btn btn-primary" type="submit">Submit</button>
							
						</div>
					</div>
				</form>
			</div>
		</section>
	</div>
</div>
<?php 
break;
case "edit":
$id=$_GET['id'];
$query="select * from tbl_itinerary where idItinerary=$id";
$proses=mysqli_query($konek,$query);
$row=mysqli_fetch_array($proses);
?>
<div class="row">
	<div class="col-lg-12">
		<section class="panel panel-primary">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a>
					<a href="#" class="fa fa-times"></a>
				</div>
				<h2 class="panel-title">Edit Itinerary</h2>
			</header>
			<div class="panel-body">
				<form class="form-horizontal form-bordered" id="form-itinerary" action="<?php echo $aksi;?>?mod=itinerary&act=edit"  method="post">
					<input type="hidden" name="namauser" value="<?php echo $_SESSION['namauser']; ?>" required>
					<input type="hidden" name="id" value="<?php echo $id; ?>" required>
					<div class="form-group">
						<label class="col-md-3 control-label">Paket</label>
						<div class="col-sm-6">
						<?php
						$query="select * from tbl_paket";
						$proses=mysqli_query($konek,$query);
						
						?>
							<select name="nama_paket" class="form-control">
								<option>---Pilih Paket---</option>
								<?php
									while ($rowe=mysqli_fetch_array($proses)) {
										if($row['idPaket'] == $rowe['idPaket']){
											?>
											<option value="<?php echo $rowe['idPaket'];?>" selected><?php echo $rowe['nmPaket'];?></option>
											<?php
										}else{
											?>
											<option value="<?php echo $rowe['idPaket'];?>"><?php echo $rowe['nmPaket'];?></option>
											<?php
										}
									}
								?>
								
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Itinerary Paket</label>
						<div class="col-md-9">
							<textarea class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }' name="itinerary_paket"><?php echo $row['Itinerary']; ?>
							</textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Created By</label>
						<div class="col-sm-6">
							<input type="text" name="sort_order" class="form-control"  value="<?php echo $row['createBy'].' - '.$row['createDate'];  ?>" disabled >
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Modified By</label>
						<div class="col-sm-6">
							<input type="text" name="sort_order" class="form-control"  value="<?php echo $row['updateBy'].' - '.$row['updateDate']; ?>" disabled >
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-6">
							<a class='btn btn-danger' href="dashboard.php?mod=itinerary" > Back</a>
							<button class="btn btn-primary" type="submit">Submit</button>
							
						</div>
					</div>
				</form>
			</div>
		</section>
	</div>
</div>
<?php
}
?>
