<?php
$aksi = "modul/mod_itinerary/mod_itinerary.php";
require( '../../config/koneksi.php' );

// storing  request (ie, get/post) global array to a variable  
$requestData= $_REQUEST;

$columns = array( 
// datatable column index  => database column name
	0 => 'idItinerary', 
	1 => 'idPaket',
	2 => 'Itinerary'
);

// getting total number records without any search
$sql = "select idItinerary, nmPaket, Itinerary from tbl_itinerary as a inner join tbl_paket as b on a.idPaket=b.idPaket";
$query=mysqli_query($konek, $sql) or die("blacklist_1");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.


$sql = "select idItinerary, nmPaket, Itinerary from tbl_itinerary as a inner join tbl_paket as b on a.idPaket=b.idPaket ";
// getting records as per search parameters
if( !empty($requestData['search']['value']) ){
	$sql.="where Itinerary like '%".$requestData['search']['value']."%' ";
	$sql.=" or nmPaket like '%".$requestData['search']['value']."%'";
}
// if( !empty($requestData['columns'][2]['search']['value']) ){
	// $sql.="WHERE noreg_jamaah = '".$requestData['columns'][2]['search']['value']."%' ";
// }

$query=mysqli_query($konek, $sql) or die("blacklist_2");
$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.

$sql.="ORDER BY idItinerary LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	
//$sql.=" GROUP BY ". $columns[$requestData['order'][0]['column']]." ORDER BY ". $columns[$requestData['order'][0]['column']]."  ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";  // adding length

$query=mysqli_query($konek, $sql) or die("blacklist_3");


$data = array();
//$idsrh = $row["srh_id"];
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
	$nestedData=array();

	$nestedData[] = $row["nmPaket"];
	$nestedData[] = $row["Itinerary"];

	$nestedData[] = '<a href="" onClick="deleteItinerary('.$row["idItinerary"].')"><i class="fa fa-trash-o"> Hapus</i></a>
					<a href="?mod=itinerary&act=edit&id='.$row["idItinerary"].'"><i class="fa fa-edit"> Edit</i></a>';
	
	$data[] = $nestedData;
}



$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);

echo json_encode($json_data);  // send data as json format

?>