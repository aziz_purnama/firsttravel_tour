<link rel="stylesheet" href="assets/vendor/summernote/summernote.css" />
<link rel="stylesheet" href="assets/vendor/summernote/summernote-bs3.css" />
<script src="assets/vendor/summernote/summernote.js"></script>
<style type="text/css">
	.container{
    margin-top:20px;
	}
	.image-preview-input {
	    position: relative;
		overflow: hidden;
		margin: 0px;    
	    color: #333;
	    background-color: #fff;
	    border-color: #ccc;    
	}
	.image-preview-input input[type=file] {
		position: absolute;
		top: 0;
		right: 0;
		margin: 0;
		padding: 0;
		font-size: 20px;
		cursor: pointer;
		opacity: 0;
		filter: alpha(opacity=0);
	}
	.image-preview-input-title {
	    margin-left:2px;
	}
</style>
<script language="javascript">
    function hanyaAngka(e, decimal) { 
    var key;
    var keychar;
     if (window.event) {
         key = window.event.keyCode;
     } else
     if (e) {
         key = e.which;
     } else return true;
    
    keychar = String.fromCharCode(key);
    if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
        return true;
    } else 
    if ((("0123456789").indexOf(keychar) > -1)) {
        return true; 
    } else 
    if (decimal && (keychar == ".")) {
        return true; 
    } else return false; 
    }
</script>

<?php 
$aksi = "modul/mod_tmbuser/action_tmbuser.php";
// mengatasi variabel yang belum di definisikan (notice undefined index)
$act = isset($_GET['act']) ? $_GET['act'] : ''; 
  switch($act){
  default:    
 ?>
<div class="row">
	<div class="col-lg-12">
		<section class="panel panel-primary">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a>
					<a href="#" class="fa fa-times"></a>
				</div>
				<h2 class="panel-title">Tambah User Baru</h2>
			</header>
			<div class="panel-body">
				<form class="form-horizontal form-bordered" id="form-paket" enctype="multipart/form-data" action="<?php echo $aksi;?>?mod=tambah-user&act=insert"  method="post">
					<div class="form-group">
						<label class="col-md-3 control-label">Username</label>
						<div class="col-sm-6">
							<input type="text" name="username" class="form-control" required="true">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Password</label>
						<div class="col-md-6">
							<input type="Password" name="pwd" class="form-control" required="true">
							</textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Nama User</label>
						<div class="col-sm-6">
							<input type="text" name="nama" class="form-control" required="true">
						</div>
						
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Email</label>
						<div class="col-sm-6">
							<input type="email" name="email" class="form-control" required="true">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-6">
							<a class='btn btn-danger' href="dashboard.php?mod=tambah-user" > Back</a>
							<button class="btn btn-primary" type="submit">Submit</button>
							
						</div>
					</div>
				</form>
			</div>
		</section>
	</div>
</div>

<?php
}
?>
	



