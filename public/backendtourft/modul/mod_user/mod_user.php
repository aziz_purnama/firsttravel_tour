<link rel="stylesheet" href="assets/vendor/summernote/summernote.css" />
<link rel="stylesheet" href="assets/vendor/summernote/summernote-bs3.css" />
<script src="assets/vendor/summernote/summernote.js"></script>
<style type="text/css">
	.container{
    margin-top:20px;
	}
	.image-preview-input {
	    position: relative;
		overflow: hidden;
		margin: 0px;    
	    color: #333;
	    background-color: #fff;
	    border-color: #ccc;    
	}
	.image-preview-input input[type=file] {
		position: absolute;
		top: 0;
		right: 0;
		margin: 0;
		padding: 0;
		font-size: 20px;
		cursor: pointer;
		opacity: 0;
		filter: alpha(opacity=0);
	}
	.image-preview-input-title {
	    margin-left:2px;
	}
</style>

<?php 
$aksi = "modul/mod_user/action_user.php";
// mengatasi variabel yang belum di definisikan (notice undefined index)
$act = isset($_GET['act']) ? $_GET['act'] : ''; 
  switch($act){
  default:  
  $id=$_SESSION['id_user'];


$sql = "select * from tbl_admin where id_admin='$id'";
$query=mysqli_query($konek, $sql) or die("blacklist_1");  
$row=mysqli_fetch_array($query);
 ?>
<div class="row">
	<div class="col-lg-12">
		<section class="panel panel-primary">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a>
					<a href="#" class="fa fa-times"></a>
				</div>
				<h2 class="panel-title">Edit User</h2>
			</header>
			<div class="panel-body">
				<form class="form-horizontal form-bordered" id="form-news" enctype="multipart/form-data">
				<input type="hidden" name="id" id="id" class="form-control" value="<?php echo $row['id_admin']; ?>">
					<div class="form-group">
						<label class="col-md-3 control-label">Email</label>
						<div class="col-sm-6">
							<input type="email" name="email" id="email" class="form-control" value="<?php echo $row['email']; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Username</label>
						<div class="col-sm-6">
							<input type="text" name="username" id="username" class="form-control" value="<?php echo $row['username']; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Display Name</label>
						<div class="col-sm-6">
							<input type="text" name="name" id="name" class="form-control" value="<?php echo $row['nama']; ?>">
						</div>
					</div>
					<div class="form-group" id="form-ind">
						<label class="col-md-3 control-label">Password</label>
						<div class="col-md-6">
							<input type="password" name="password" id="password" class="form-control" >
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-6">
							<a class='btn btn-danger' href="dashboard.php?mod=user" > Cancel</a>
							<button class="btn btn-primary" type="button" id="done">Submit</button>
							
						</div>
					</div>
				</form>
			</div>
		</section>
	</div>
</div>

<!--Modal Loading -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      	<div class="modal-content">
        	<div class="modal-header">
          		<h4 class="modal-title">Data berhasil diubah!</h4>
        	</div>
	        <div class="modal-body">
	          	<p>Data anda telah berhasil diubah! Terimakasih.</p>	
	        </div>
      	</div>
    </div>
</div>

<script type="text/javascript">

$(document).ready(function() {
	//Modal Loading
	var waitingDialog = waitingDialog || (function ($) {
	    'use strict';

		// Creating modal dialog's DOM
		var $dialog = $(
			'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
			'<div class="modal-dialog modal-m">' +
			'<div class="modal-content">' +
				'<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
				'<div class="modal-body">' +
					'<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
				'</div>' +
			'</div></div></div>');

		return {
			/**
			 * Opens our dialog
			 * @param message Custom message
			 * @param options Custom options:
			 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
			 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
			 */
			show: function (message, options) {
				// Assigning defaults
				if (typeof options === 'undefined') {
					options = {};
				}
				if (typeof message === 'undefined') {
					message = 'Loading';
				}
				var settings = $.extend({
					dialogSize: 'm',
					progressType: '',
					onHide: null // This callback runs after the dialog was hidden
				}, options);

				// Configuring dialog
				$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
				$dialog.find('.progress-bar').attr('class', 'progress-bar');
				if (settings.progressType) {
					$dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
				}
				$dialog.find('h3').text(message);
				// Adding callbacks
				if (typeof settings.onHide === 'function') {
					$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
						settings.onHide.call($dialog);
					});
				}
				// Opening dialog
				$dialog.modal();
			},
			/**
			 * Closes dialog
			 */
			hide: function () {
				$dialog.modal('hide');
			}
		};

	})(jQuery);

//Ketika tombol Validasi Di klik
	$('#done').click(function(){   
		var id 			= $('#id').val();
		var email 		= $('#email').val();
		var username 	= $('#username').val();
		var name 		= $('#name').val();
		var password 	= $('#password').val();

	$.ajax({
			type: "POST",
			dataType: "json",
			url:"modul/mod_user/action_user.php",
			data: {id:id, email:email, username,username, name:name, password:password},
			beforeSend: function() {
				// setting a timeout
				console.log("error");
				waitingDialog.show();
			},
			success: function(result) {	
				waitingDialog.hide();
				$("#myModal").modal("show");
				$("#done").hide();
			}

			});
	});
	
	$('#myModal').click(function(){ 
		window.location="dashboard.php?mod=user";
	});

});
</script>
<?php
}
?>
	



