<?php
  include "../../config/koneksi.php";
  include "../../config/library.php";


$module = $_GET['mod'];
$act    = $_GET['act'];


if ($module=='paket' AND $act=='insert'){
	$username 			= $_POST['namauser'];
	$date 				= date('Y-m-d H:i:s');
	$nama_paket 		= $_POST['nama_paket'];
	$deskrip_paket 		= $_POST['deskrip_paket'];
	$harga_paket 		= $_POST['harga_paket'];
	$exp_date 			= $_POST['exp_date'];
	$kategori_paket 	= $_POST['kategori_paket'];
	$itinerary_paket 	= $_POST['itinerary_paket'];

	//format penyimpanan foto
	$lokasi_file_foto = $_FILES['uploadfoto']['tmp_name'];
    $tipe_file_foto   = $_FILES['uploadfoto']['type'];
    $nama_file_foto   = $_FILES['uploadfoto']['name'];
    $file_size_foto   = $_FILES['uploadfoto']['size'];
    $file_error_foto  = $_FILES['uploadfoto']['error'];

	$tgl_input		= date('YmdHis');
    $acak           = rand(2, 999);
	$path			= "assets/img/tour/";
    $file_foto      = $path.$nama_file_foto;

    if ($lokasi_file_foto!='') {
			    $allowedExtsFOTO = array("gif", "jpeg", "jpg", "png");
				$extensionFOTO = explode(".", $nama_file_foto);
				$extensionFOTO_jadi = end($extensionFOTO);
				if (($tipe_file_foto == "image/gif")
				|| ($tipe_file_foto == "image/jpeg")
				|| ($tipe_file_foto == "image/jpg")
				|| ($tipe_file_foto == "image/png")
				|| ($tipe_file_foto == "image/x-png")
				&& ($file_size_foto < 200000)
				&& in_array($extensionFOTO_jadi, $allowedExtsFOTO))
				{
				if ($file_error_foto > 0)
				{
				$msgphoto ="Foto = Upload error Code: " . $file_error_foto . "<br>";
				}
				else
				{
				$msgphoto = "Foto = Berhasil di upload";
// echo "../../assets/".$nama_foto;
				if (file_exists("../../../assets/img/tour/".$nama_file_foto))
				  {
				  $msgphoto = "Foto = Sudah pernah di upload";
				  }
				else
				  {
				  move_uploaded_file($lokasi_file_foto,
				  "../../../assets/img/tour/".$nama_file_foto );
				  $msgphoto ="Foto = Berhasil di upload" ;
				  }
				 }
				 }
				else
				{
				$msgphoto="Foto = Format tidak mendukung";
				 }
				 echo $msgphoto;
	}
	
	$strquery = "INSERT INTO tbl_paket (nmPaket, desPaket, kategoriPaket, hrgPaket, file_foto, expPaket, createBy, createDate) VALUES 
				('$nama_paket', '$deskrip_paket', '$kategori_paket', '$harga_paket','$file_foto', '$exp_date', '$username', '$date')";
	mysqli_query($konek,$strquery);

	$query="select * from tbl_paket order by idPaket desc limit 1;";
	$proses=mysqli_query($konek,$query);
	$row=mysqli_fetch_array($proses);
	$idPaket = $row['idPaket'];

	$strquerye = "INSERT INTO tbl_itinerary (idPaket, Itinerary, createBy, createDate) VALUES 
				('$idPaket', '$itinerary_paket', '$username', '$date')";
    mysqli_query($konek,$strquerye);

	
	// echo "<script>alert('Data Berhasil diinsert'); </script>";
	header("location:../../dashboard.php?mod=$module");
}
else if ($module=='paket' AND $act=='edit'){
	$id 				= $_POST['id'];
	$username 			= $_POST['namauser'];
	$date 				= date('Y-m-d H:i:s');
	$nama_paket 		= $_POST['nama_paket'];
	$deskrip_paket 		= $_POST['deskrip_paket'];
	$harga_paket 		= $_POST['harga_paket'];
	$exp_date 			= $_POST['exp_date'];
	$kategori_paket 	= $_POST['kategori_paket'];
	$itinerary_paket 	= $_POST['itinerary_paket'];

	//format penyimpanan foto
	$lokasi_file_foto = $_FILES['uploadfoto']['tmp_name'];
    $tipe_file_foto   = $_FILES['uploadfoto']['type'];
    $nama_file_foto   = $_FILES['uploadfoto']['name'];
    $file_size_foto   = $_FILES['uploadfoto']['size'];
    $file_error_foto  = $_FILES['uploadfoto']['error'];

	$tgl_input		= date('YmdHis');
    $acak           = rand(2, 999);
	$path			= "assets/img/tour/";
    $file_foto      = $path.$nama_file_foto;

    if ($lokasi_file_foto!='') {
			    $allowedExtsFOTO = array("gif", "jpeg", "jpg", "png");
				$extensionFOTO = explode(".", $nama_file_foto);
				$extensionFOTO_jadi = end($extensionFOTO);
				if (($tipe_file_foto == "image/gif")
				|| ($tipe_file_foto == "image/jpeg")
				|| ($tipe_file_foto == "image/jpg")
				|| ($tipe_file_foto == "image/png")
				|| ($tipe_file_foto == "image/x-png")
				&& ($file_size_foto < 200000)
				&& in_array($extensionFOTO_jadi, $allowedExtsFOTO))
				{
				if ($file_error_foto > 0)
				{
				$msgphoto ="Foto = Upload error Code: " . $file_error_foto . "<br>";
				}
				else
				{
				$msgphoto = "Foto = Berhasil di upload";
// echo "../../assets/".$nama_foto;
				if (file_exists("../../../assets/img/tour/".$nama_file_foto))
				  {
				  $msgphoto = "Foto = Sudah pernah di upload";
				  }
				else
				  {
				  move_uploaded_file($lokasi_file_foto,
				  "../../../assets/img/tour/".$nama_file_foto );
				  $msgphoto ="Foto = Berhasil di upload" ;
				  }
				 }
				 }
				else
				{
				$msgphoto="Foto = Format tidak mendukung";
				 }
				 echo $msgphoto;
	}
	if($nama_file_foto == ''){
		$strquery = "UPDATE tbl_paket
				SET nmPaket='$nama_paket', desPaket='$deskrip_paket', kategoriPaket='$kategori_paket', hrgPaket='$harga_paket', expPaket='$exp_date', updateBy='$username', updateDate='$date'
				WHERE idPaket='$id'";

	mysqli_query($konek,$strquery);
	}else{
		$strquery = "UPDATE tbl_paket
				SET nmPaket='$nama_paket', desPaket='$deskrip_paket', file_foto='$file_foto', kategoriPaket='$kategori_paket', hrgPaket='$harga_paket', expPaket='$exp_date', updateBy='$username', updateDate='$date'
				WHERE idPaket='$id'";
				
	mysqli_query($konek,$strquery);
	}
	
	$strquerye = "UPDATE tbl_itinerary
				SET Itinerary='$itinerary_paket', updateBy='$username', updateDate='$date'
				WHERE idPaket='$id'";
    mysqli_query($konek,$strquerye);

				
   
	// echo "<script>alert('Data Berhasil diinsert'); </script>";
	header("location:../../dashboard.php?mod=$module");
}
?>