<link rel="stylesheet" href="assets/vendor/summernote/summernote.css" />
<link rel="stylesheet" href="assets/vendor/summernote/summernote-bs3.css" />
<script src="assets/vendor/summernote/summernote.js"></script>
<style type="text/css">
	.container{
    margin-top:20px;
	}
	.image-preview-input {
	    position: relative;
		overflow: hidden;
		margin: 0px;    
	    color: #333;
	    background-color: #fff;
	    border-color: #ccc;    
	}
	.image-preview-input input[type=file] {
		position: absolute;
		top: 0;
		right: 0;
		margin: 0;
		padding: 0;
		font-size: 20px;
		cursor: pointer;
		opacity: 0;
		filter: alpha(opacity=0);
	}
	.image-preview-input-title {
	    margin-left:2px;
	}
</style>
<script language="javascript">
    function hanyaAngka(e, decimal) { 
    var key;
    var keychar;
     if (window.event) {
         key = window.event.keyCode;
     } else
     if (e) {
         key = e.which;
     } else return true;
    
    keychar = String.fromCharCode(key);
    if ((key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) {
        return true;
    } else 
    if ((("0123456789").indexOf(keychar) > -1)) {
        return true; 
    } else 
    if (decimal && (keychar == ".")) {
        return true; 
    } else return false; 
    }
</script>

<?php 
$aksi = "modul/mod_paket/action_paket.php";
// mengatasi variabel yang belum di definisikan (notice undefined index)
$act = isset($_GET['act']) ? $_GET['act'] : ''; 
  switch($act){
  default:    
 ?>
 <div class="tab-content">
<div id="jamaah" class="tab-pane fade in active">
<div class="row">
	<div class="col-lg-12">
		<section class="panel panel-primary">
			<header class="panel-heading">
				<div class="panel-actions">
					<div class="mb-md">
						<button id="add_page" class="btn btn-primary" onclick="location.href = '?mod=paket&act=insert';">Add <i class="fa fa-plus"></i></button>
					</div>
				</div>
				<h2 class="panel-title">Paket Tour</h2>

			</header>
			<div class="panel-body">
			<div class="row">
					
				</div>
				<table class="table table-bordered table-striped table-hover mb-none" id="dataTable-paket" >
					<thead>
					  <tr>
						<th>Nama Paket</th>
						<th>Harga Paket</th>
						<th>Exp Paket</th>
						<th>Kategori Paket</th>
						<th>Actions</th>
					  </tr>
					</thead>
				</table>
			</div>
		</section>
	</div>
</div>
</div>

</div>
<script>
	var dataTable;
	$(document).ready(function() {
		dataTable = $('#dataTable-paket').DataTable( {
			"processing": true,
			"serverSide": true,
			"ajax": "modul/mod_paket/json_paket.php"
		});
		
	});

	function deletePaket(idPaket){
		$.ajax({
			type: "POST",
			url: "modul/mod_paket/delete_paket.php",
			dataType: 'json',
			data: {id:idPaket},
			beforeSend: function(){
     			alert("Apakah ingin menghapus data ini?");
  			},
			success: function(data) {
				alert("Data Berhasil dihapus");
				dataTable.ajax.reload();
			}
		});
	}
</script>
 <?php
  break;
  case "insert":
?>


<div class="row">
	<div class="col-lg-12">
		<section class="panel panel-primary">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a>
					<a href="#" class="fa fa-times"></a>
				</div>
				<h2 class="panel-title">Tambah Paket Baru</h2>
			</header>
			<div class="panel-body">
				<form class="form-horizontal form-bordered" id="form-paket" enctype="multipart/form-data" action="<?php echo $aksi;?>?mod=paket&act=insert"  method="post">
					<input type="hidden" name="namauser" value="<?php echo $_SESSION['namauser']; ?>" required>
					<div class="form-group">
						<label class="col-md-3 control-label">Nama Paket</label>
						<div class="col-sm-6">
							<input type="text" name="nama_paket" class="form-control" required="true">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Deskripsi Paket</label>
						<div class="col-md-9">
							<textarea class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }' name="deskrip_paket" required="true">
							</textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Harga</label>
						<div class="col-sm-6">
							<input type="text" name="harga_paket" class="form-control" onkeypress="return hanyaAngka(event, false)" required="true">
							<span style="color:red; ">*tidak perlu memakai tanda titik(.) atau tanda koma(,)</span>
						</div>
						
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Kategori Paket</label>
						<div class="col-sm-6">
							<select name="kategori_paket" class="form-control" required="true">
								<option>---Pilih Kategori---</option>
								<option value="Internasional">Internasional</option>
								<option value="Domestik">Domestik</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Masa Berlaku Paket (Exp Date)</label>
						<div class="col-sm-6">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
								<input type="text" name="exp_date" data-plugin-datepicker  placeholder="YYYY-MM-DD" class="form-control" required>
							</div>
						</div>
					</div>
					<div class="form-group">
	                	<label class="col-md-3 control-label" for="kode_merchant">Upload Foto</label>
				        <div class="col-xs-12 col-md-6 col-sm-8 ">  
				            <!-- image-preview-filename input [CUT FROM HERE]-->
				            <div class="input-group image-preview">
				                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
				                <span class="input-group-btn">
				                    <!-- image-preview-clear button -->
				                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
				                        <span class="glyphicon glyphicon-remove"></span> Clear
				                    </button>
				                    <!-- image-preview-input -->
				                    <div class="btn btn-default image-preview-input">
				                        <span class="glyphicon glyphicon-folder-open"></span>
				                        <input type="file" accept="image/png, image/jpeg" id="logo-image" max-size='200000' required name="uploadfoto"/> <!-- rename it -->
				                    </div>
				                </span>
				            </div><!-- /input-group image-preview [TO HERE]--> 
				        </div>	
			        </div>
					 <div class="row" id="error" style="display:none">   
				    	<div class="col-xs-12 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2"> 
					    	<label style="color:red"> Max Size 200 KB</label>
					    </div>
				    </div>
				    <div class="form-group">
						<label class="col-md-3 control-label">Itinerary Paket</label>
						<div class="col-md-9">
							<textarea class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }' name="itinerary_paket">
							</textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-6">
							<a class='btn btn-danger' href="dashboard.php?mod=paket" > Back</a>
							<button class="btn btn-primary" type="submit">Submit</button>
							
						</div>
					</div>
				</form>
			</div>
		</section>
	</div>
</div>

<script type="text/javascript">
	$(document).on('click', '#close-preview', function(){ 

		    $('.image-preview').popover('hide');
		    // Hover befor close the preview
		    $('.image-preview').hover(
		        function () {
		           $('.image-preview').popover('show');
		        }, 
		         function () {
		           $('.image-preview').popover('hide');
		        }
		    );    
	});

	$(function() {
	
	    // Create the close button
	    var closebtn = $('<button/>', {
	        type:"button",
	        text: 'x',
	        id: 'close-preview',
	        style: 'font-size: initial;',
	    });
	    closebtn.attr("class","close pull-right");
	    // Set the popover default content
	    $('.image-preview').popover({
	        trigger:'manual',
	        html:true,
	        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
	        content: "There's no image",
	        placement:'left'
	    });
	    // Clear event
	    $('.image-preview-clear').click(function(){
	        $('.image-preview').attr("data-content","").popover('hide');
	        $('.image-preview-filename').val("");
	        $('.image-preview-clear').hide();
	        $('.image-preview-input input:file').val("");
	        $(".image-preview-input-title").text("Browse"); 
	    }); 
	    // Create the preview image
	    $(".image-preview-input input:file").change(function (){     
	        var img = $('<img/>', {
	            id: 'dynamic',
	            width:250,
	            height:200
	        });      
	        var file = this.files[0];
	        var reader = new FileReader();
	        // Set preview image into the popover data-content
	        reader.onload = function (e) {
	            $(".image-preview-input-title").text("Change");
	            $(".image-preview-clear").show();
	            $(".image-preview-filename").val(file.name);            
	            img.attr('src', e.target.result);
	            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
	        }        
	        reader.readAsDataURL(file);
	    }); 
	    $('form').submit(function(){
		        var isOk = true;
		        $('input[type=file][max-size]').each(function(){
		            if(typeof this.files[0] !== 'undefined'){
		                var maxSize = parseInt($(this).attr('max-size'),10),
		                size = this.files[0].size;
		                isOk = maxSize > size;
		                console.log(this.files[0].size,maxSize,isOk);
		                return isOk;
		            }
		        });
		        if(isOk == false){
		        	$('#error').show();
		        	setTimeout(function () {
		                $('#error').hide();
		            }, 3000);
		        	return isOk;
		        }
		    });
	}); 
</script>
<?php 
break;
case "edit":
$id=$_GET['id'];
$query="select * from tbl_paket where idPaket=$id";
$proses=mysqli_query($konek,$query);
$row=mysqli_fetch_array($proses);
$idPaket = $row['idPaket'];

$querye="select * from tbl_itinerary where idPaket=$idPaket";
$prosese=mysqli_query($konek,$querye);
$rowe=mysqli_fetch_array($prosese);
?>

<div class="row">
	<div class="col-lg-12">
		<section class="panel panel-primary">
			<header class="panel-heading">
				<div class="panel-actions">
					<a href="#" class="fa fa-caret-down"></a>
					<a href="#" class="fa fa-times"></a>
				</div>
				<h2 class="panel-title">Edit Paket</h2>
			</header>
			<div class="panel-body">
				<form class="form-horizontal form-bordered" id="form-paket" enctype="multipart/form-data" action="<?php echo $aksi;?>?mod=paket&act=edit"  method="post">
					<input type="hidden" name="namauser" value="<?php echo $_SESSION['namauser']; ?>" required>
					<input type="hidden" name="id" value="<?php echo $row['idPaket']; ?>" required>
					<div class="form-group">
						<label class="col-md-3 control-label">Nama Paket</label>
						<div class="col-sm-6">
							<input type="text" name="nama_paket" class="form-control" value="<?php echo $row['nmPaket']; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Deskripsi Paket</label>
						<div class="col-md-9">
							<textarea class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }' name="deskrip_paket"><?php echo $row['desPaket']; ?>
							</textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Harga</label>
						<div class="col-sm-6">
							<input type="text" name="harga_paket" class="form-control" value="<?php echo $row['hrgPaket']; ?>" onkeypress="return hanyaAngka(event, false)" >
							<span style="color:red; ">*tidak perlu memakai tanda titik(.) atau tanda koma(,)</span>
						</div>	
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Kategori Paket</label>
						<div class="col-sm-6">
							<select name="kategori_paket" class="form-control">
								<option>---Pilih Kategori---</option>
								<?php if($row['kategoriPaket'] == 'Internasional'){ ?>
								<option value="Internasional" selected>Internasional</option>
								<option value="Domestik">Domestik</option>
								<?php } else{?>
								<option value="Domestik" selected>Domestik</option>
								<option value="Internasional">Internasional</option>
								<?php } ?>
								
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Masa Berlaku Paket (Exp Date)</label>
						<div class="col-sm-6">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
								<input type="text" value="<?php echo $row['expPaket']; ?>" name="exp_date" data-plugin-datepicker  placeholder="YYYY-MM-DD" class="form-control" required >
							</div>
						</div>
					</div>
					<div class="form-group">
	                	<label class="col-md-3 control-label" for="kode_merchant">Upload Foto</label>
				        <div class="col-xs-12 col-md-6 col-sm-8 ">  
				            <!-- image-preview-filename input [CUT FROM HERE]-->
				            <div class="input-group image-preview">
				                <input type="text" value="<?php echo $row['file_foto']; ?>" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
				                <span class="input-group-btn">
				                    <!-- image-preview-clear button -->
				                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
				                        <span class="glyphicon glyphicon-remove"></span> Clear
				                    </button>
				                    <!-- image-preview-input -->
				                    <div class="btn btn-default image-preview-input">
				                        <span class="glyphicon glyphicon-folder-open"></span>
				                        <input type="file" accept="image/png, image/jpeg" id="logo-image" max-size='200000' name="uploadfoto"/> <!-- rename it -->
				                    </div>
				                </span>
				            </div><!-- /input-group image-preview [TO HERE]--> 
				            <span><a class='modal-sizes' href="#modalRJ">
       							<button class="mb-xs mt-xs mr-xs btn btn-primary" type="button">Preview</button></a>
       						</span>
				        </div>	
			        </div>
					<div class="row" id="error" style="display:none">   
				    	<div class="col-xs-12 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2"> 
					    	<label style="color:red"> Max Size 200 KB</label>
					    </div>
				    </div>
				       <div class="form-group">
						<label class="col-md-3 control-label">Itinerary Paket</label>
						<div class="col-md-9">
							<textarea class="summernote" data-plugin-summernote data-plugin-options='{ "height": 180, "codemirror": { "theme": "ambiance" } }' name="itinerary_paket"><?php echo $rowe['Itinerary']; ?>
							</textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Create By</label>
						<div class="col-sm-6">
							<input type="text" name="create_by" class="form-control" value="<?php echo $row['createBy'].' - '.$row['createDate']; ?>" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Modified By</label>
						<div class="col-sm-6">
							<input type="text" name="modified_by" class="form-control" value="<?php echo $row['updateBy'].' - '.$row['updateDate']; ?>" >
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-6">
							<a class='btn btn-danger' href="dashboard.php?mod=paket" > Back</a>
							<button class="btn btn-primary" type="submit">Submit</button>
							
						</div>
					</div>
				</form>
			</div>
		</section>
	</div>
</div>

<!-- Modal content -->
<div id="modalRJ" class="modal-block modal-block-sm mfp-hide">
    <section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Foto Paket</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-icon center">
					<img src="../<?php echo $row['file_foto'] ?>" width="250" height="200">
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button class="btn btn-default modal-dismiss">OK</button>
				</div>
			</div>
		</footer>
	</section>
</div>
<script type="text/javascript">
	$(document).on('click', '#close-preview', function(){ 

	    $('.image-preview').popover('hide');
	    // Hover befor close the preview
	    $('.image-preview').hover(
	        function () {
	           $('.image-preview').popover('show');
	        }, 
	         function () {
	           $('.image-preview').popover('hide');
	        }
	    );    
	});

	$(function() {

	    // Create the close button
	    var closebtn = $('<button/>', {
	        type:"button",
	        text: 'x',
	        id: 'close-preview',
	        style: 'font-size: initial;',
	    });
	    closebtn.attr("class","close pull-right");
	    // Set the popover default content
	    $('.image-preview').popover({
	        trigger:'manual',
	        html:true,
	        title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
	        content: "There's no image",
	        placement:'left'
	    });
	    // Clear event
	    $('.image-preview-clear').click(function(){
	        $('.image-preview').attr("data-content","").popover('hide');
	        $('.image-preview-filename').val("");
	        $('.image-preview-clear').hide();
	        $('.image-preview-input input:file').val("");
	        $(".image-preview-input-title").text("Browse"); 
	    }); 
	    // Create the preview image
	    $(".image-preview-input input:file").change(function (){     
	        var img = $('<img/>', {
	            id: 'dynamic',
	            width:250,
	            height:200
	        });      
	        var file = this.files[0];
	        var reader = new FileReader();
	        // Set preview image into the popover data-content
	        reader.onload = function (e) {
	            $(".image-preview-input-title").text("Change");
	            $(".image-preview-clear").show();
	            $(".image-preview-filename").val(file.name);            
	            img.attr('src', e.target.result);
	            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
	        }        
	        reader.readAsDataURL(file);
	    });

	     $('form').submit(function(){
	        var isOk = true;
	        $('input[type=file][max-size]').each(function(){
	            if(typeof this.files[0] !== 'undefined'){
	                var maxSize = parseInt($(this).attr('max-size'),10),
	                size = this.files[0].size;
	                isOk = maxSize > size;
	                console.log(this.files[0].size,maxSize,isOk);
	                return isOk;
	            }
	        });
	        if(isOk == false){
	        	$('#error').show();
	        	setTimeout(function () {
	                $('#error').hide();
	            }, 3000);
	        	return isOk;
	        }
		}); 
	}); 
</script>
<?php
}
?>
	



