-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.5.50-MariaDB - MariaDB Server
-- Server OS:                    Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for firsttravel_tour
CREATE DATABASE IF NOT EXISTS `firsttravel_tour` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `firsttravel_tour`;

-- Dumping structure for table firsttravel_tour.tbl_admin
CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `pwd` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `level` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `lastlogin` datetime NOT NULL,
  `id_session` varchar(100) DEFAULT NULL,
  `id_cabang` int(11) DEFAULT NULL,
  `printer_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table firsttravel_tour.tbl_admin: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_admin` ENABLE KEYS */;

-- Dumping structure for table firsttravel_tour.tbl_fotopaket
CREATE TABLE IF NOT EXISTS `tbl_fotopaket` (
  `id_foto` int(11) NOT NULL AUTO_INCREMENT,
  `nama_foto` varchar(255) NOT NULL,
  `file_foto` varchar(255) NOT NULL,
  `id_paket` varchar(50) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_by` varchar(255) NOT NULL,
  `update_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_foto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table firsttravel_tour.tbl_fotopaket: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_fotopaket` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_fotopaket` ENABLE KEYS */;

-- Dumping structure for table firsttravel_tour.tbl_itinerary
CREATE TABLE IF NOT EXISTS `tbl_itinerary` (
  `idItinerary` int(11) NOT NULL AUTO_INCREMENT,
  `idPaket` int(11) NOT NULL,
  `Itinerary` varchar(500) NOT NULL,
  `createBy` varchar(50) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `updateBy` varchar(50) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  PRIMARY KEY (`idItinerary`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table firsttravel_tour.tbl_itinerary: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_itinerary` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_itinerary` ENABLE KEYS */;

-- Dumping structure for table firsttravel_tour.tbl_modul
CREATE TABLE IF NOT EXISTS `tbl_modul` (
  `id_modul` int(3) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) NOT NULL,
  `nama_modul` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `icons` varchar(100) NOT NULL,
  `urutan` int(3) NOT NULL,
  `aktif` enum('Y','N') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id_modul`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table firsttravel_tour.tbl_modul: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_modul` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_modul` ENABLE KEYS */;

-- Dumping structure for table firsttravel_tour.tbl_paket
CREATE TABLE IF NOT EXISTS `tbl_paket` (
  `idPaket` int(11) NOT NULL AUTO_INCREMENT,
  `nmPaket` varchar(200) DEFAULT NULL,
  `desPaket` varchar(500) DEFAULT NULL,
  `hrgPaket` double DEFAULT NULL,
  `expPaket` datetime DEFAULT NULL,
  `createBy` varchar(50) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `updateBy` varchar(50) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  PRIMARY KEY (`idPaket`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table firsttravel_tour.tbl_paket: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_paket` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_paket` ENABLE KEYS */;

-- Dumping structure for table firsttravel_tour.tbl_Transaksi
CREATE TABLE IF NOT EXISTS `tbl_Transaksi` (
  `idTrans` int(11) NOT NULL AUTO_INCREMENT,
  `noTrans` varchar(20) NOT NULL,
  `idPaket` int(11) NOT NULL,
  `nmPaket` varchar(200) NOT NULL,
  `jmlPax` int(11) NOT NULL,
  `nmTraveler` varchar(200) NOT NULL,
  `dobTraveler` datetime NOT NULL,
  `idTraveler` varchar(100) NOT NULL,
  `passTraveler` varchar(100) DEFAULT NULL,
  `emailTraveler` varchar(100) NOT NULL,
  `tlpTraveler` varchar(20) NOT NULL,
  `hrgPaketSatuan` double DEFAULT NULL,
  `hrgTotal` double NOT NULL,
  `jnsPembyaran` varchar(20) NOT NULL,
  `statusPembayaran` int(11) DEFAULT NULL,
  `createBy` varchar(50) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `updateBy` varchar(50) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  PRIMARY KEY (`idTrans`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table firsttravel_tour.tbl_Transaksi: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_Transaksi` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_Transaksi` ENABLE KEYS */;

-- Dumping structure for table firsttravel_tour.tbl_TransaksiDetail
CREATE TABLE IF NOT EXISTS `tbl_TransaksiDetail` (
  `idTransDet` int(11) NOT NULL AUTO_INCREMENT,
  `idTrans` int(11) NOT NULL,
  `nmTravelerDet` varchar(200) NOT NULL,
  `dobTravelerDet` datetime NOT NULL,
  `createBy` varchar(50) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `updateBy` varchar(50) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  PRIMARY KEY (`idTransDet`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table firsttravel_tour.tbl_TransaksiDetail: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_TransaksiDetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_TransaksiDetail` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
